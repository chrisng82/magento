<?php

namespace Merchstack\RedeemEwallet\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            // Add ref id column
            $setup->getConnection()->addColumn(
                    $setup->getTable('redeem_ewallet'),
                    'ref_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => '11',
                        'default' => 0,
                        'nullable' => false,
                        'comment' => 'Reference Id',
                        'after' => 'trx_type'
                    ]
            );
        }

        $installer->endSetup();
    }

}
