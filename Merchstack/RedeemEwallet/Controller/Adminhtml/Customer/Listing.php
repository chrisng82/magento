<?php

namespace Merchstack\RedeemEwallet\Controller\Adminhtml\Customer;

class Listing extends \Magento\Backend\App\Action {

    protected $authSession;
    protected $resultPageFactory;
    protected $customerFactory;
    protected $ewalletFactory;
    protected $dateTime;

    public function __construct(
            \Magento\Backend\Model\Auth\Session $authSession,
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTime) {
        parent::__construct($context);
        $this->authSession = $authSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerFactory = $customerFactory;
        $this->ewalletFactory = $ewalletFactory;
        $this->dateTime = $dateTime;
    }

    public function execute() {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Merchstack_Ewallet::customer_listing');
        $resultPage->addBreadcrumb(__('Customers'), __('Customers'));
        $resultPage->addBreadcrumb(__('Online Customers'), __('Online Customers'));
        $resultPage->getConfig()->getTitle()->prepend(__('Redeem Ewallet Transactions'));
        return $resultPage;
    }

}

?>