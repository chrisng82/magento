<?php

namespace Merchstack\RedeemEwallet\Block;

class Template extends \Magento\Framework\View\Element\Template {

    protected $util;

    public function getPeriodDisplay($period) {
        return $this->util->getPeriodDisplay($period);
    }

    public function getPeriodValue($period) {
        return $this->util->getPeriodValue($period);
    }

    public function getFromDate($period) {
        return $this->util->getFromDate($period);
    }

    public function getToDate($period) {
        return $this->util->getToDate($period);;
    }

    public function formatAmt($amt) {
        return $this->util->formatAmt($amt);
    }

}
