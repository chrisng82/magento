<?php

namespace Merchstack\RedeemEwallet\Block\Customer;

class Index extends \Merchstack\RedeemEwallet\Block\Template {

    protected $customerSession;
    protected $redeemEwalletRepo;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\RedeemEwallet\Model\RedeemEwalletRepository $redeemEwalletRepo,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\RedeemEwallet\Helper\Util $util) {
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->redeemEwalletRepo = $redeemEwalletRepo;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomer($memberId) {
        if (!empty($memberId)) {
            $customer = $this->customerFactory->create();
            $customers = $customer->getCollection()->addAttributeToFilter('member_id', $memberId);
            return $customers->getSize() > 0 ? $customers->getFirstItem() : null;
        } else {
            return null;
        }
    }

    public function getRedeemBalance() {
        $customer = $this->customerSession->getCustomer();
        return $this->redeemEwalletRepo->getBalance($customer->getId());
    }

}
