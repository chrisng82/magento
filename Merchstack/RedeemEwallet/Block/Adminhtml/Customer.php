<?php

namespace Merchstack\RedeemEwallet\Block\Adminhtml;

class Customer extends \Merchstack\RedeemEwallet\Block\Template {

    protected $ewalletRepo;
    protected $redeemEwalletFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Merchstack\RedeemEwallet\Model\RedeemEwalletRepository $redeemEwalletRepo,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\RedeemEwallet\Helper\Util $util) {
        $this->customerFactory = $customerFactory;
        $this->redeemEwalletRepo = $redeemEwalletRepo;
        $this->ewalletFactory = $ewalletFactory;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('id');
    }

    public function getCustomer() {
        $id = $this->getCustomerId();
        if (!empty($id)) {
            $customer = $this->customerFactory->create();
            $customer->load($id);
            return $customer;
        } else {
            return null;
        }
    }

    public function getBalance() {
        return $this->redeemEwalletRepo->getBalance($this->getCustomerId());
    }

    public function getBalanceBefore($period) {
        if ($period == null) {
            return 0;
        }

        $from = $this->getFromDate($period);
        return $this->redeemEwalletRepo->getBalanceBefore($this->getCustomerId(), $from);
    }

    public function getPeriods() {
        return $this->redeemEwalletRepo->findPeriods($this->getCustomerId());
    }

    public function getTransactions($period) {
        if ($period == null) {
            return [];
        }

        $from = $this->getFromDate($period);
        $to = $this->getToDate($period);
        return $this->redeemEwalletRepo->findByCustomerIdAndPeriod($this->getCustomerId(), $from, $to);
    }

}
