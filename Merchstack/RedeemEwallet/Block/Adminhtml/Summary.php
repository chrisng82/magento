<?php

namespace Merchstack\RedeemEwallet\Block\Adminhtml;

class Summary extends \Merchstack\RedeemEwallet\Block\Template implements \Magento\Ui\Component\Layout\Tabs\TabInterface {

    protected $_template = 'customer/summary.phtml';
    protected $redeemEwalletRepo;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Merchstack\Ewallet\Model\EwalletRepository $ewalletRepo,
            \Merchstack\RedeemEwallet\Model\RedeemEwalletRepository $redeemEwalletRepo,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\RedeemEwallet\Helper\Util $util) {
        $this->customerFactory = $customerFactory;
        $this->ewalletRepo = $ewalletRepo;
        $this->redeemEwalletRepo = $redeemEwalletRepo;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('id');
    }

    public function getCustomer() {
        $id = $this->getCustomerId();
        if (!empty($id)) {
            $customer = $this->customerFactory->create();
            $customer->load($id);
            return $customer;
        } else {
            return null;
        }
    }

    public function getCashBalance() {
        return $this->ewalletRepo->getBalance($this->getCustomerId());
    }

    public function getPendingWithdrawal() {
        return $this->ewalletRepo->getPendingWithdrawal($this->getCustomerId());
    }

    public function getRedeemBalance() {
        return $this->redeemEwalletRepo->getBalance($this->getCustomerId());
    }

    public function getTabLabel() {
        return __('Ewallet');
    }

    public function getTabTitle() {
        return __('Ewallet');
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }

    public function getTabClass() {
        return '';
    }

    public function getTabUrl() {
        return '';
    }

    public function isAjaxLoaded() {
        return false;
    }

}
