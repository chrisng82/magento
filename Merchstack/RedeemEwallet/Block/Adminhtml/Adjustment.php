<?php

namespace Merchstack\RedeemEwallet\Block\Adminhtml;

use \Merchstack\RedeemEwallet\Model\RedeemEwalletType;

class Adjustment extends \Merchstack\RedeemEwallet\Block\Template {

    protected $customerSession;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\RedeemEwallet\Helper\Util $util) {
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('id');
    }

    public function getCustomer() {
        $id = $this->getCustomerId();
        if (!empty($id)) {
            $customer = $this->customerFactory->create();
            $customer->load($id);
            return $customer;
        } else {
            return null;
        }
    }

    public function getAdjTypes() {
        return [
            RedeemEwalletType::ADJUSTMENT_IN,
            RedeemEwalletType::ADJUSTMENT_OUT
        ];
    }
    
    public function getAdjTypeLabel($type) {
        return RedeemEwalletType::getTypeLabel($type);
    }
}
