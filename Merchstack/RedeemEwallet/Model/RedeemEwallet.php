<?php

namespace Merchstack\RedeemEwallet\Model;

class RedeemEwallet extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'merchstack_redeemewallet_ewallet';

    protected function _construct() {
        $this->_init('Merchstack\RedeemEwallet\Model\ResourceModel\RedeemEwallet');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];
        return $values;
    }

}
