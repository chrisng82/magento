<?php

namespace Merchstack\RedeemEwallet\Model\ResourceModel\RedeemEwallet;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct() {
        $this->_init('Merchstack\RedeemEwallet\Model\RedeemEwallet',
                'Merchstack\RedeemEwallet\Model\ResourceModel\RedeemEwallet');
    }
}
