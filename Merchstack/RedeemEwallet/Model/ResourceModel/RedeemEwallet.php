<?php

namespace Merchstack\RedeemEwallet\Model\ResourceModel;

class RedeemEwallet extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('redeem_ewallet', 'id');
    }
}
