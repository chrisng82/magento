<?php

namespace Merchstack\RedeemEwallet\Model;

class RedeemEwalletType {
    const ADJUSTMENT_IN = "AI";
    const ADJUSTMENT_OUT = "AO";
    const SHARED_CONTENT_IN = 'SCI';
    const CONTENT_SHARED_IN = 'CSI';

    public static function getTypeLabel($type) {
        $label = null;
        if ($type === self::ADJUSTMENT_IN) {
            $label = "Adjustment In";
        } else if ($type == self::ADJUSTMENT_OUT) {
            $label = "Adjustment Out";
        } else if ($type == self::SHARED_CONTENT_IN) {
            $label = "Shared Content In";
        } else if ($type == self::CONTENT_SHARED_IN) {
            $label = "Content Shared In";
        }
        return $label;
    }
}
