<?php

namespace Merchstack\RedeemEwallet\Model;

class RedeemEwalletRepository {

    protected $resourceConnection;
    protected $redeemEwalletFactory;

    public function __construct(
            \Magento\Framework\App\ResourceConnection $resourceConnection,
            \Merchstack\RedeemEwallet\Model\RedeemEwalletFactory $redeemEwalletFactory) {
        $this->redeemEwalletFactory = $redeemEwalletFactory;
        $this->resourceConnection = $resourceConnection;
    }

    public function findByCustomerIdAndPeriod($customerId, $from, $to) {
        $ewallet = $this->redeemEwalletFactory->create();
        $collection = $ewallet->getCollection();
        $collection->getSelect()
                ->where('owner_id = ?', $customerId)
                ->where('stmt_date >= ?', $from)
                ->where('stmt_date <= ?', $to);
        return $collection;
    }

    public function findPeriods($customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT distinct MONTH(stmt_date) as month, YEAR(stmt_date) as year ' .
                'FROM redeem_ewallet ' .
                'WHERE owner_id = ' . $customerId . ' ' .
                'ORDER BY stmt_date DESC';
        return $connection->fetchAll($query);
    }

    public function getBalance($customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT SUM(amt_in - amt_out) as balance ' .
                'FROM redeem_ewallet ' .
                'WHERE owner_id = ' . $customerId;
        $results = $connection->fetchOne($query);
        return $results !== null ? $results : 0;
    }

    public function getBalanceBefore($customerId, $before) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT SUM(amt_in - amt_out) as balance ' .
                'FROM redeem_ewallet ' .
                'WHERE owner_id = ' . $customerId . ' ' .
                'AND stmt_date < "' . $before . '"';
        $results = $connection->fetchOne($query);
        return $results !== null ? $results : 0;
    }

}
