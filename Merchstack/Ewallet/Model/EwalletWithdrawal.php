<?php

namespace Merchstack\Ewallet\Model;

class EwalletWithdrawal extends \Magento\Framework\Model\AbstractModel 
    implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'merchstack_ewallet_ewallet_withdrawal';

    const PENDING = 0;
    const REJECTED = 1;
    const APPROVED = 2;
    
    protected function _construct() {
        $this->_init('Merchstack\Ewallet\Model\ResourceModel\EwalletWithdrawal');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];
        return $values;
    }
}
