<?php

namespace Merchstack\Ewallet\Model;

class EwalletRepository implements \Merchstack\Ewallet\Api\EwalletRepositoryInterface {

    protected $resourceConnection;
    protected $ewalletFactory;
    protected $ewalletWithdrawalFactory;
    protected $eavAttribute;

    public function __construct(
            \Magento\Framework\App\ResourceConnection $resourceConnection,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Merchstack\Ewallet\Model\EwalletWithdrawalFactory $ewalletWithdrawalFactory,
            \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute) {
        $this->resourceConnection = $resourceConnection;
        $this->ewalletFactory = $ewalletFactory;
        $this->ewalletWithdrawalFactory = $ewalletWithdrawalFactory;
        $this->eavAttribute = $eavAttribute;
    }

    public function findByCustomerIdAndPeriod($customerId, $from, $to) {
        $ewallet = $this->ewalletFactory->create();
        $collection = $ewallet->getCollection();
        $collection->getSelect()
                ->where('owner_id = ?', $customerId)
                ->where('stmt_date >= ?', $from)
                ->where('stmt_date <= ?', $to);
        return $collection;
    }

    public function findPeriods($customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT distinct MONTH(stmt_date) as month, YEAR(stmt_date) as year ' .
                'FROM ewallet ' .
                'WHERE owner_id = ' . $customerId . ' ' .
                'ORDER BY stmt_date DESC';
        return $connection->fetchAll($query);
    }

    public function getBalance($customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT SUM(amt_in - amt_out) as balance ' .
                'FROM ewallet ' .
                'WHERE owner_id = ' . $customerId;
        $results = $connection->fetchOne($query);
        return $results !== null ? $results : 0;
    }

    public function getBalanceBefore($customerId, $before) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT SUM(amt_in - amt_out) as balance ' .
                'FROM ewallet ' .
                'WHERE owner_id = ' . $customerId . ' ' .
                'AND stmt_date < "' . $before . '"';
        $results = $connection->fetchOne($query);
        return $results !== null ? $results : 0;
    }

    public function getPendingWithdrawal($customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT SUM(amt) as balance ' .
                'FROM ewallet_withdrawal ' .
                'WHERE owner_id = ' . $customerId . ' ' .
                'AND status = 0';
        $results = $connection->fetchOne($query);
        return $results !== null ? $results : 0;
    }

}
