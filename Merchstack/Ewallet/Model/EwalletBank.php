<?php

namespace Merchstack\Ewallet\Model;

class EwalletBank {

    public static function getBanks() {
        return [
            [
                'code' => 'CIBB',
                'desc' => 'CIMB'
            ],
            [
                'code' => 'MBB',
                'desc' => 'MAYBANK'
            ],
        ];
    }

    public static function getBank($code) {
        foreach (self::getBanks() as $bank) {
            if ($bank['code'] == $code) {
                return $bank;
            }
        }

        return null;
    }

}
