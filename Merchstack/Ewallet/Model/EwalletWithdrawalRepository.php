<?php

namespace Merchstack\Ewallet\Model;

class EwalletWithdrawalRepository implements \Merchstack\Ewallet\Api\EwalletRepositoryInterface {

    protected $resourceConnection;
    protected $ewalletFactory;
    protected $ewalletWithdrawalFactory;
    protected $eavAttribute;

    public function __construct(
            \Magento\Framework\App\ResourceConnection $resourceConnection,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Merchstack\Ewallet\Model\EwalletWithdrawalFactory $ewalletWithdrawalFactory,
            \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute) {
        $this->resourceConnection = $resourceConnection;
        $this->ewalletFactory = $ewalletFactory;
        $this->ewalletWithdrawalFactory = $ewalletWithdrawalFactory;
        $this->eavAttribute = $eavAttribute;
    }

    public function findByCustomerIdAndPeriod($customerId, $from, $to, $status = -1) {
        $ewallet = $this->ewalletWithdrawalFactory->create();
        $collection = $ewallet->getCollection();

        $select = $collection->getSelect();
        $select->where('owner_id = ?', $customerId)
                ->where('stmt_date >= ?', $from)
                ->where('stmt_date <= ?', $to);

        if ($status >= 0) {
            $select->where('status = ?', $status);
        }

        return $collection;
    }

    public function findByPeriod($from, $to, $status) {
        $ewallet = $this->ewalletWithdrawalFactory->create();
        $collection = $ewallet->getCollection();
        $select = $collection->getSelect();
        $select->where('stmt_date >= ?', $from)
                ->where('stmt_date <= ?', $to);

        if ($status >= 0) {
            $select->where('status = ?', $status);
        }
        return $collection;
    }

    public function findAllPeriods() {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT distinct MONTH(stmt_date) as month, YEAR(stmt_date) as year ' .
                'FROM ewallet_withdrawal ' .
                'ORDER BY stmt_date DESC';
        return $connection->fetchAll($query);
    }

    public function findPeriods($customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT distinct MONTH(stmt_date) as month, YEAR(stmt_date) as year ' .
                'FROM ewallet_withdrawal ' .
                'WHERE owner_id = ' . $customerId . ' ' .
                'ORDER BY stmt_date DESC';
        return $connection->fetchAll($query);
    }

}
