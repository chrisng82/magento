<?php

namespace Merchstack\Ewallet\Model\ResourceModel\Ewallet;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct() {
        $this->_init('Merchstack\Ewallet\Model\Ewallet',
                'Merchstack\Ewallet\Model\ResourceModel\Ewallet');
    }
}
