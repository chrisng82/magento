<?php

namespace Merchstack\Ewallet\Model\ResourceModel;

class EwalletWithdrawal extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('ewallet_withdrawal', 'id');
    }
}
