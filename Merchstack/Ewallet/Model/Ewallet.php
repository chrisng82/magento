<?php

namespace Merchstack\Ewallet\Model;

class Ewallet extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'merchstack_ewallet_ewallet';

    protected function _construct() {
        $this->_init('Merchstack\Ewallet\Model\ResourceModel\Ewallet');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];
        return $values;
    }
}
