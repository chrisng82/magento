<?php

namespace Merchstack\Ewallet\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            // Sales Invoice

            $setup->getConnection()->addColumn(
                    $setup->getTable('ewallet'),
                    'doneby',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '30',
                        'nullable' => false,
                        'comment' => 'Done by',
                        'after' => 'status'
                    ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            // Fix data type

            $setup->getConnection()->changeColumn(
                    $setup->getTable('ewallet_withdrawal'),
                    'process_by',
                    'process_by',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '30',
                        'nullable' => false,
                        'comment' => 'Process By'
                    ]
            );
        }

        $installer->endSetup();
    }

}
