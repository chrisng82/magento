<?php

namespace Merchstack\Ewallet\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        /**
         * Create table 'ewallet'
         */
        $table = $setup->getConnection()
                ->newTable($setup->getTable('ewallet'))
                ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'ID'
                )
                ->addColumn(
                        'owner_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        10,
                        ['nullable' => false],
                        'Owner Id'
                )
                ->addColumn(
                        'trx_type',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        3,
                        ['nullable' => false],
                        'Transaction Type'
                )
                ->addColumn(
                        'trx_date',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false],
                        'Transaction Date'
                )
                ->addColumn(
                        'stmt_date',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                        null,
                        ['nullable' => false],
                        'Date'
                )
                ->addColumn(
                        'amt_in',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '12,4',
                        ['default' => 0.00],
                        'Amount In'
                )
                ->addColumn(
                        'amt_out',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '12,4',
                        ['default' => 0.00],
                        'Amount Out'
                )
                ->addColumn(
                        'remark',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        null,
                        ['nullable => false'],
                        'Remark'
                )
                ->addColumn(
                        'status',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        1,
                        ['nullable' => false],
                        'status'
                )
                ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Created At'
                )
                ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Updated At'
        );
        $setup->getConnection()->createTable($table);

        /**
         * Create table 'ewallet_withdrawal'
         */
        $table = $setup->getConnection()
                ->newTable($setup->getTable('ewallet_withdrawal'))
                ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'ID'
                )
                ->addColumn(
                        'owner_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        10,
                        ['nullable' => false],
                        'Owner Id'
                )
                ->addColumn(
                        'stmt_date',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                        null,
                        ['nullable' => false],
                        'Date'
                )
                ->addColumn(
                        'amt',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '12,4',
                        ['default' => 0.00],
                        'Amount'
                )
                ->addColumn(
                        'rate',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '12,4',
                        ['default' => 0.00],
                        'Amount'
                )
                ->addColumn(
                        'remark',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        null,
                        ['nullable => false'],
                        'Remark'
                )
                ->addColumn(
                        'acct_no',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        50,
                        ['nullable => false'],
                        'Account No.'
                )
                ->addColumn(
                        'acct_name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        50,
                        ['nullable => false'],
                        'Account Name'
                )
                ->addColumn(
                        'bank_code',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        10,
                        ['nullable => false'],
                        'Bank Code'
                )
                ->addColumn(
                        'status',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        1,
                        ['nullable' => false],
                        'status'
                )
                ->addColumn(
                        'process_date',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                        null,
                        ['nullable' => true, 'default' => null],
                        'Process Date'
                )
                ->addColumn(
                        'process_by',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        10,
                        ['nullable' => true, 'default' => null],
                        'Process By'
                )
                ->addColumn(
                        'process_remark',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        null,
                        ['nullable => false'],
                        'Process Remark'
                )
                ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Created At'
                )
                ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Updated At'
        );

        $setup->getConnection()->createTable($table);
    }

}
