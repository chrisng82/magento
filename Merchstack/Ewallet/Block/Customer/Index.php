<?php

namespace Merchstack\Ewallet\Block\Customer;

class Index extends \Merchstack\Ewallet\Block\Template {

    protected $customerSession;
    protected $ewalletRepo;
    protected $ewalletFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\Ewallet\Model\EwalletRepository $ewalletRepo,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\Ewallet\Helper\Util $util) {
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->ewalletRepo = $ewalletRepo;
        $this->ewalletFactory = $ewalletFactory;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getPendingWithdrawal() {
        $customer = $this->customerSession->getCustomer();
        return $this->ewalletRepo->getPendingWithdrawal($customer->getId());
    }

    public function getBalance() {
        $customer = $this->customerSession->getCustomer();
        return $this->ewalletRepo->getBalance($customer->getId());
    }

    public function getBalanceBefore($period) {
        $customer = $this->customerSession->getCustomer();

        if ($period == null) {
            return 0;
        }

        $from = $this->getFromDate($period);
        return $this->ewalletRepo->getBalanceBefore($customer->getId(), $from);
    }

    public function getPeriods() {
        $customer = $this->customerSession->getCustomer();
        return $this->ewalletRepo->findPeriods($customer->getId());
    }

    public function getTransactions($period) {
        $customer = $this->customerSession->getCustomer();

        if ($period == null) {
            return [];
        }

        $to = $this->getFromDate($period);
        $to = $this->getToDate($period);
        return $this->ewalletRepo->findByCustomerIdAndPeriod($customer->getId(), $from, $to);
    }
}
