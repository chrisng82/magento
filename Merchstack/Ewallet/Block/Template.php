<?php

namespace Merchstack\Ewallet\Block;

class Template extends \Magento\Framework\View\Element\Template {

    protected $util;

    public function getPeriodDisplay($period) {
        return $this->util->getPeriodDisplay($period);
    }

    public function getPeriodValue($period) {
        return $this->util->getPeriodValue($period);
    }

    public function formatAmt($amt) {
        return $this->util->formatAmt($amt);
    }

    public function getFromDate($period) {
        return $this->util->getFromDate($period);
    }

    public function getToDate($period) {
        return $this->util->getToDate($period);
    }

    public function getBanks() {
        return \Merchstack\Ewallet\Model\EwalletBank::getBanks();
    }

    public function getBank($code) {
        return \Merchstack\Ewallet\Model\EwalletBank::getBank($code);
    }

    public function getCashValue($amt) {
        return $this->util->getCashValue($amt);
    }

    public function getCashRate() {
        return $this->util->getCashRate();
    }

}
