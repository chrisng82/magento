<?php

namespace Merchstack\Ewallet\Block\Adminhtml\Customer;

class Adjustment extends \Merchstack\Ewallet\Block\Template {

    protected $customerSession;
    protected $spTreeFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\NetworkTree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\Ewallet\Helper\Util $util) {
        $this->customerSession = $customerSession;
        $this->spTreeFactory = $spTreeFactory;
        $this->customerFactory = $customerFactory;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('id');
    }

    public function getCustomer() {
        $id = $this->getCustomerId();
        if (!empty($id)) {
            $customer = $this->customerFactory->create();
            $customer->load($id);
            return $customer;
        } else {
            return null;
        }
    }

}
