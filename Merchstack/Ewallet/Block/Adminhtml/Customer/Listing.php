<?php

namespace Merchstack\Ewallet\Block\Adminhtml\Customer;

class Listing extends \Merchstack\Ewallet\Block\Template {

    protected $ewalletRepo;
    protected $ewalletFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Merchstack\Ewallet\Model\EwalletRepository $ewalletRepo,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\Ewallet\Helper\Util $util) {
        $this->customerFactory = $customerFactory;
        $this->ewalletRepo = $ewalletRepo;
        $this->ewalletFactory = $ewalletFactory;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('id');
    }

    public function getCustomer() {
        $id = $this->getCustomerId();
        if (!empty($id)) {
            $customer = $this->customerFactory->create();
            $customer->load($id);
            return $customer;
        } else {
            return null;
        }
    }

    public function getBalance() {
        return $this->ewalletRepo->getBalance($this->getCustomerId());
    }

    public function getBalanceBefore($period) {
        if ($period == null) {
            return 0;
        }

        $from = $this->getFromDate($period);
        return $this->ewalletRepo->getBalanceBefore($this->getCustomerId(), $from);
    }

    public function getPeriods() {
        return $this->ewalletRepo->findPeriods($this->getCustomerId());
    }

    public function getTransactions($period) {
        if ($period == null) {
            return [];
        }

        $from = $this->getFromDate($period);
        $to = $this->getToDate($period);
        return $this->ewalletRepo->findByCustomerIdAndPeriod($this->getCustomerId(), $from, $to);
    }

}
