<?php

namespace Merchstack\Ewallet\Block\Adminhtml\Withdrawal;

class Listing extends \Merchstack\Ewallet\Block\WithdrawalTemplate {

    protected $ewalletWithdrawalRepo;
    protected $ewalletFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Merchstack\Ewallet\Model\EwalletWithdrawalRepository $ewalletWithdrawalRepo,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\Ewallet\Helper\Util $util) {
        $this->customerFactory = $customerFactory;
        $this->ewalletWithdrawalRepo = $ewalletWithdrawalRepo;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('id');
    }

    public function getCustomer() {
        $id = $this->getCustomerId();
        if (!empty($id)) {
            $customer = $this->customerFactory->create();
            $customer->load($id);
            return $customer;
        } else {
            return null;
        }
    }

    public function getPeriods() {
        if ($this->getCustomerId() !== null) {
            return $this->ewalletWithdrawalRepo->findPeriods($this->getCustomerId());
        } else {
            return $this->ewalletWithdrawalRepo->findAllPeriods();
        }
    }

    public function findWithdrawals($period, $status) {
        if ($period == null) {
            return [];
        }

        $input = explode('-', $period);
        $from = $input[1] . '-' . $input[0] . '-1';
        $to = date("Y-m-t", strtotime($from));


        if ($this->getCustomerId() !== null) {
            return $this->ewalletWithdrawalRepo->findByCustomerIdAndPeriod($this->getCustomerId(), $from, $to, $status);
        } else {
            return $this->ewalletWithdrawalRepo->findByPeriod($from, $to, $status);
        }
    }

}
