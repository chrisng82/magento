<?php

namespace Merchstack\Ewallet\Block\Adminhtml\Withdrawal;

class Process extends \Merchstack\Ewallet\Block\WithdrawalTemplate {

    const REJECT = \Merchstack\Ewallet\Model\EwalletWithdrawal::REJECTED; 
    const APPROVE = \Merchstack\Ewallet\Model\EwalletWithdrawal::APPROVED;

    protected $ewalletWithdrawalRepo;
    protected $ewalletWithdrawalFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Merchstack\Ewallet\Model\EwalletWithdrawalFactory $ewalletWithdrawalFactory,
            \Merchstack\Ewallet\Helper\Util $util) {
        $this->ewalletWithdrawalFactory = $ewalletWithdrawalFactory;
                $this->util = $util;
        return parent::__construct($context);
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('customer_id');
    }

    public function getWithdrawalId() {
        return $this->getRequest()->getParam('id');
    }

    public function getWithdrawal() {
        $withdrawal = $this->ewalletWithdrawalFactory->create();
        return $withdrawal->load($this->getWithdrawalId());
    }

}
