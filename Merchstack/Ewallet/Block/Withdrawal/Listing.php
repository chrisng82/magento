<?php

namespace Merchstack\Ewallet\Block\Withdrawal;

class Listing extends \Merchstack\Ewallet\Block\WithdrawalTemplate {

    protected $customerSession;
    protected $ewalletWithdrawalRepo;
    protected $ewalletWithdrawalFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\Ewallet\Model\EwalletWithdrawalRepository $ewalletWithdrawalRepo,
            \Merchstack\Ewallet\Model\EwalletWithdrawalFactory $ewalletWithdrawalFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\Ewallet\Helper\Util $util) {
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->ewalletWithdrawalRepo = $ewalletWithdrawalRepo;
        $this->ewalletWithdrawalFactory = $ewalletWithdrawalFactory;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getPeriods() {
        $customer = $this->customerSession->getCustomer();
        return $this->ewalletWithdrawalRepo->findPeriods($customer->getId());
    }

    public function getTransactions($period) {
        $customer = $this->customerSession->getCustomer();

        if ($period == null) {
            return [];
        }

        $from = $this->util->getFromDate($period);
        $to = $this->util->getToDate($period);
        return $this->ewalletWithdrawalRepo->findByCustomerIdAndPeriod($customer->getId(), $from, $to);
    }

    public function getBank($code) {
        return \Merchstack\Ewallet\Model\EwalletBank::getBank($code);
    }

}
