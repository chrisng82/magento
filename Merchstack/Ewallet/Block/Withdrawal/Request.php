<?php

namespace Merchstack\Ewallet\Block\Withdrawal;

class Request extends \Merchstack\Ewallet\Block\WithdrawalTemplate {

    protected $customerSession;
    protected $ewalletRepo;
    protected $ewalletFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\Ewallet\Model\EwalletRepository $ewalletRepo,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\Ewallet\Helper\Util $util) {
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->ewalletRepo = $ewalletRepo;
        $this->ewalletFactory = $ewalletFactory;
        $this->util = $util;
        return parent::__construct($context);
    }

    public function getBalance() {
        $customer = $this->customerSession->getCustomer();
        return $this->ewalletRepo->getBalance($customer->getId());
    }

}
