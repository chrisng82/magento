<?php

namespace Merchstack\Ewallet\Block;

class WithdrawalTemplate extends \Merchstack\Ewallet\Block\Template {

    protected $statuses;

    public function getStatus($status) {
        if ($status == \Merchstack\Ewallet\Model\EwalletWithdrawal::PENDING) {
            return "Pending";
        } else if ($status == \Merchstack\Ewallet\Model\EwalletWithdrawal::REJECTED) {
            return "Rejected";
        } else if ($status == \Merchstack\Ewallet\Model\EwalletWithdrawal::APPROVED) {
            return "Approved";
        }
    }

    public function getStatusOptions() {
        if (!$this->statuses) {
            $values = [
                \Merchstack\Ewallet\Model\EwalletWithdrawal::PENDING,
                \Merchstack\Ewallet\Model\EwalletWithdrawal::REJECTED,
                \Merchstack\Ewallet\Model\EwalletWithdrawal::APPROVED
            ];

            $this->statuses = [];
            array_push($this->statuses, ['label' => 'All', 'value' => -1]);

            foreach ($values as $value) {
                array_push($this->statuses, ['label' => $this->getStatus($value), 'value' => $value]);
            }
        }

        return $this->statuses;
    }

}
