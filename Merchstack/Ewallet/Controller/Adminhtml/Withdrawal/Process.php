<?php

namespace Merchstack\Ewallet\Controller\Adminhtml\Withdrawal;

class Process extends \Magento\Backend\App\Action {

    const RATE = 3;

    protected $authSession;
    protected $resultPageFactory;
    protected $customerFactory;
    protected $ewalletRepo;
    protected $ewalletFactory;
    protected $ewalletWithdrawalFactory;
    protected $dateTime;

    public function __construct(
            \Magento\Backend\Model\Auth\Session $authSession,
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Merchstack\Ewallet\Model\EwalletRepository $ewalletRepo,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\Ewallet\Model\EwalletWithdrawalFactory $ewalletWithdrawalFactory,
            \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTime) {
        parent::__construct($context);
        $this->authSession = $authSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->ewalletRepo = $ewalletRepo;
        $this->customerFactory = $customerFactory;
        $this->ewalletFactory = $ewalletFactory;
        $this->ewalletWithdrawalFactory = $ewalletWithdrawalFactory;
        $this->dateTime = $dateTime;
    }

    public function execute() {
        $process = false;

        if ($this->getRequest()->getParams() !== null) {
            $withdrawal = $this->parseWithdrawal();
            if ($withdrawal !== null) {
                $withdrawal->save();

                $this->deductEwallet($withdrawal);

                $process = true;
            }
        }

        if ($process) {
            $params = [
                'id' => $this->getRequest()->getParam('id')
            ];

            $this->messageManager->addSuccess(__('Withdrawal updated.'));
            $this->_redirect('ewallet/withdrawal/process', $params);
        } else {
            $resultPage = $this->resultPageFactory->create();
            $resultPage->setActiveMenu('Merchstack_Ewallet::ewallet_withdrawal_process');
            $resultPage->addBreadcrumb(__('Customers'), __('Customers'));
            $resultPage->addBreadcrumb(__('Online Customers'), __('Online Customers'));
            $resultPage->getConfig()->getTitle()->prepend(__('Process Ewallet Withdrawal'));
            return $resultPage;
        }
    }

    protected function parseWithdrawal() {
        $id = $this->getRequest()->getParam('id');
        $action = $this->getRequest()->getParam('action');
        $processRemark = $this->getRequest()->getParam('process_remark');

        if ($action == '') {
            return null;
        }

        $withdrawal = $this->ewalletWithdrawalFactory->create();
        $withdrawal->load($id);

        if ($withdrawal !== null) {
            if ($withdrawal->getData('status') != 0) {
                $this->messageManager->addError(__('Withdrawal already approved/rejected.'));
                return null;
            }

            $balance = $this->ewalletRepo->getBalance($withdrawal->getData('owner_id')) * self::RATE;
            if ($action == \Merchstack\Ewallet\Model\EwalletWithdrawal::APPROVED && $balance < $withdrawal->getData('amt')) {
                $this->messageManager->addError(__('Insufficient balance for withdrawal.'));
                return null;
            }

            $stmtDate = $this->dateTime->date()->format('Y-m-d');

            $withdrawal->setData('status', $action);
            $withdrawal->setData('process_remark', $processRemark);
            $withdrawal->setData('process_by', $this->authSession->getUser()->getUsername());
            $withdrawal->setData('process_date', $stmtDate);
            return $withdrawal;
        } else {
            return null;
        }
    }

    protected function deductEwallet($withdrawal) {
        if ($withdrawal->getData('status') == \Merchstack\Ewallet\Model\EwalletWithdrawal::APPROVED) {
            $ewallet = $this->ewalletFactory->create();
            $ewallet->setData('owner_id', $withdrawal->getData('owner_id'));
            $ewallet->setData('trx_type', 'WO');
            $ewallet->setData('trx_date', $withdrawal->getData('updated_at'));
            $ewallet->setData('stmt_date', $withdrawal->getData('process_date'));
            $ewallet->setData('amt_out', $withdrawal->getData('amt') / self::RATE);
            $ewallet->setData('remark', 'Withdrawal approved');
            $ewallet->setData('doneby', $withdrawal->getData('process_by'));
            $ewallet->save();
        }
    }
}
