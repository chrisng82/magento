<?php

namespace Merchstack\Ewallet\Controller\Adminhtml\Customer;

class Adjustment extends \Magento\Backend\App\Action {

    protected $authSession;
    protected $resultPageFactory;
    protected $customerFactory;
    protected $ewalletFactory;
    protected $dateTime;

    public function __construct(
            \Magento\Backend\Model\Auth\Session $authSession,
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTime) {
        parent::__construct($context);
        $this->authSession = $authSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerFactory = $customerFactory;
        $this->ewalletFactory = $ewalletFactory;
        $this->dateTime = $dateTime;
    }

    public function execute() {
        $adjId = null;

        if ($this->getRequest()->getParams() !== null) {
            $ewallet = $this->parseEwallet();
            if ($ewallet !== null) {
                $ewallet->save();
                $adjId = $ewallet->getId();
            }
        }

        if ($adjId !== null) {
            $params = [
                'id' => $this->getCustomerId()
            ];

            $this->messageManager->addSuccess(__('Adjustment saved.'));
            $this->_redirect('ewallet/customer/listing', $params);
        } else {
            $resultPage = $this->resultPageFactory->create();
            $resultPage->setActiveMenu('Merchstack_Ewallet::customer_adjustment');
            $resultPage->addBreadcrumb(__('Customers'), __('Customers'));
            $resultPage->addBreadcrumb(__('Online Customers'), __('Online Customers'));
            $resultPage->getConfig()->getTitle()->prepend(__('Ewallet Adjustment'));
            return $resultPage;
        }
    }

    public function getCustomerId() {
        return $this->getRequest()->getParam('id');
    }

    public function getCustomer() {
        $id = $this->getCustomerId();
        if (!empty($id)) {
            $customer = $this->customerFactory->create();
            $customer->load($id);
            return $customer;
        } else {
            return null;
        }
    }

    protected function parseEwallet() {
        $trxType = $this->getRequest()->getParam('trxType');
        $amount = $this->getRequest()->getParam('amount');
        $remark = $this->getRequest()->getParam('remark');

        if (!empty($trxType) && !empty($remark)) {

            $amountIn = 0;
            $amountOut = 0;

            if ($trxType == 'AI') {
                $amountIn = $amount;
            } else if ($trxType == 'AO') {
                $amountOut = $amount;
            }

            $stmtDate = $this->dateTime->date()->format('Y-m-d');
            $ewallet = $this->ewalletFactory->create();
            $ewallet->setData('owner_id', $this->getCustomerId());
            $ewallet->setData('trx_type', $trxType);
            $ewallet->setData('stmt_date', $stmtDate);
            $ewallet->setData('amt_in', $amountIn);
            $ewallet->setData('amt_out', $amountOut);
            $ewallet->setData('remark', $remark);
            $ewallet->setData('doneby', $this->authSession->getUser()->getUsername());
            return $ewallet;
        }

        return null;
    }

}

?>