<?php

namespace Merchstack\Ewallet\Controller\Withdrawal;

class Listing extends \Magento\Framework\App\Action\Action {
    protected $pageFactory;
    protected $customerSession;
    protected $urlInterface;
    
    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\Framework\UrlInterface $urlInterface) {
        $this->pageFactory = $pageFactory;
        $this->customerSession = $customerSession;
        $this->urlInterface = $urlInterface;
        return parent::__construct($context);
    }

    public function execute() {
        if (!$this->customerSession->isLoggedIn()) {
            $this->customerSession->setAfterAuthUrl($this->urlInterface->getCurrentUrl());
            $this->customerSession->authenticate();
        } else {
            return $this->pageFactory->create();
        }
    }
}
