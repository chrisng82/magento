<?php

namespace Merchstack\Ewallet\Controller\Withdrawal;

class Request extends \Magento\Framework\App\Action\Action {

    protected $pageFactory;
    protected $customerSession;
    protected $ewalletRepo;
    protected $ewalletWithdrawalFactory;
    protected $urlInterface;
    protected $dateTime;
    protected $util;

    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\Ewallet\Model\EwalletRepository $ewalletRepo,
            \Merchstack\Ewallet\Model\EwalletWithdrawalFactory $ewalletWithdrawalFactory,
            \Magento\Framework\UrlInterface $urlInterface,
            \Merchstack\Ewallet\Helper\Util $util,
            \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTime) {
        $this->pageFactory = $pageFactory;
        $this->customerSession = $customerSession;
        $this->ewalletRepo = $ewalletRepo;
        $this->ewalletWithdrawalFactory = $ewalletWithdrawalFactory;
        $this->urlInterface = $urlInterface;
                $this->util = $util;
        $this->dateTime = $dateTime;
        return parent::__construct($context);
    }

    public function execute() {
        if (!$this->customerSession->isLoggedIn()) {
            $this->customerSession->setAfterAuthUrl($this->urlInterface->getCurrentUrl());
            $this->customerSession->authenticate();
        } else {
            $ewId = 0;

            $customer = $this->customerSession->getCustomer();

            if ($this->getRequest()->getParam('submit') !== null) {
                $acctName = $this->getRequest()->getParam('acctName');
                $acctNo = $this->getRequest()->getParam('acctNo');
                $amt = $this->getRequest()->getParam('amt');
                $bankCode = $this->getRequest()->getParam('bankCode');
                $remark = $this->getRequest()->getParam('remark');
                $stmtDate = $this->dateTime->date()->format('Y-m-d');
                
                $balance = $this->ewalletRepo->getBalance($customer->getId());
                if ($amt > $this->util->getCashValue($balance)) {
                    $this->messageManager->addError(__('Ewallet balance not sufficient.'));
                } else {
                    $ewalletWithdrawal = $this->ewalletWithdrawalFactory->create();
                    $ewalletWithdrawal->setData('owner_id', $customer->getId());
                    $ewalletWithdrawal->setData('stmt_date', $stmtDate);
                    $ewalletWithdrawal->setData('acct_no', $acctNo);
                    $ewalletWithdrawal->setData('acct_name', $acctName);
                    $ewalletWithdrawal->setData('bank_code', $bankCode);
                    $ewalletWithdrawal->setData('amt', $amt);
                    $ewalletWithdrawal->setData('remark', $remark);

                    $ewalletWithdrawal->save();
                    $ewId = $ewalletWithdrawal->getId();
                }
            }

            if ($ewId > 0) {
                $this->messageManager->addSuccess(__('Withdrawal request saved.'));
                $this->_redirect('ewallet/withdrawal/listing');
            } else {
                return $this->pageFactory->create();
            }
        }
    }

}
