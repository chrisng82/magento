<?php

namespace Merchstack\Ewallet\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Util extends AbstractHelper {

    const PERIOD_FORMAT = 'F Y';
    const AMOUNT_DECIMAL = 2;
    const CASH_EWALLET_RATE = 3;

    public function getPeriodDisplay($period) {
        return date(self::PERIOD_FORMAT, mktime(0, 0, 0, $period['month'], 1, $period['year']));
    }

    public function getPeriodValue($period) {
        return $period['month'] . '-' . $period['year'];
    }

    public function formatAmt($amt) {
        return number_format($amt, self::AMOUNT_DECIMAL);
    }

    public function getFromDate($period) {
        $input = explode('-', $period);
        $from = $input[1] . '-' . $input[0] . '-1';
        return $from;
    }

    public function getToDate($period) {
        $input = explode('-', $period);
        $from = $input[1] . '-' . $input[0] . '-1';
        $to = date("Y-m-t", strtotime($from));
        return $to;
    }

    public function getCashValue($amt) {
        return $amt * self::CASH_EWALLET_RATE;
    }
    
    public function getCashRate() {
        return self::CASH_EWALLET_RATE;
    }
}
