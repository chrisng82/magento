<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Merchstack\Shipping\Model\ResourceModel\Order\Shipment\Grid;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    /**
     * Initialize dependencies.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'sales_shipment_grid',
        $resourceModel = \Magento\Sales\Model\ResourceModel\Order\Shipment::class
    ) {
        $this->logger = $logger;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    protected $logger;

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinLeft(
            ['track' => $this->getTable('sales_shipment_track')],
            'main_table.entity_id = track.parent_id',
            ['shipping_tracking' => new \Zend_Db_Expr('CONCAT_WS(" ", track.title, track.track_number)')]
        );

        $this->addFilterToMap(
            'shipping_tracking',
            new \Zend_Db_Expr('CONCAT_WS(" ", track.title, track.track_number)')
        );
        $this->addFilterToMap('entity_id', 'main_table.entity_id');

        $this->logger->addError($this->getSelect());

        return $this;
    }
}
