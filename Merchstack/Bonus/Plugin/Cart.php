<?php

namespace Merchstack\Bonus\Plugin;

class Cart {
    protected $cart;

    public function __construct(
            \Magento\Checkout\Model\Cart $cart) {
        $this->cart = $cart;
    }

    public function afterGetSectionData(\Magento\Checkout\CustomerData\Cart $subject, array $result) {
        $items = [];
        foreach ($result['items'] as $item) {
            array_push($items, $item);
        }

        $result['total_pv'] = $this->cart->getQuote()->getData('total_pv');
        $result['items'] = $items;
        return $result;
    }
}

?>