<?php

namespace Merchstack\Bonus\Plugin;

use Magento\Quote\Model\Quote\Item;

class DefaultItem {

    public function aroundGetItemData(\Magento\Checkout\CustomerData\DefaultItem $subject,
            \Closure $proceed,
            Item $item) {
        $result = $proceed($item);
        $result['pv'] = $item['pv'];
        $result['total_pv'] = $item['total_pv'];
        return $result;
    }

}

?>