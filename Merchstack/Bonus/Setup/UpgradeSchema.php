<?php

namespace Merchstack\Bonus\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            // Sales Invoice

            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_invoice'),
                    'total_pv',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,4',
                        'default' => 0,
                        'nullable' => false,
                        'comment' => 'Total PV'
                    ]
            );
            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_invoice_item'),
                    'pv',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,4',
                        'default' => 0,
                        'nullable' => false,
                        'comment' => 'PV'
                    ]
            );
            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_invoice_item'),
                    'total_pv',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,4',
                        'default' => 0,
                        'nullable' => false,
                        'comment' => 'Total PV'
                    ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            // Sales Invoice

            $setup->getConnection()->addColumn(
                    $setup->getTable('sales_invoice'),
                    'bonus_status',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => '3',
                        'default' => 0,
                        'nullable' => false,
                        'comment' => 'Bonus Status'
                    ]
            );
        }
        
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $setup->getConnection()->dropTable($setup->getTable('direct_bonus'));
            /**
             * Create table 'direct_bonus'
             */
            $table = $installer->getConnection()
                    ->newTable($setup->getTable('direct_bonus'))
                    ->addColumn(
                            'id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                            'ID'
                    )
                    ->addColumn(
                            'date',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                            null,
                            ['nullable' => false],
                            'Date'
                    )
                    ->addColumn(
                            'customer_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            10,
                            ['nullable' => false],
                            'Customer Id'
                    )
                    ->addColumn(
                            'from_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            10,
                            ['nullable' => false],
                            'From Id'
                    )
                    ->addColumn(
                            'item_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            10,
                            ['nullable' => false],
                            'Parent'
                    )
                    ->addColumn(
                            'pv',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                            '12,4',
                            ['default' => 0.00],
                            'pv'
                    )
                    ->addColumn(
                            'level',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            10,
                            ['nullable' => false],
                            'Level'
                    )
                    ->addColumn(
                            'bonus',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                            '12,4',
                            ['default' => 0.00],
                            'Bonus'
                    )
                    ->addColumn(
                            'ref_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            10,
                            ['nullable' => false],
                            'Ref Id'
                    )
                    ->addColumn(
                            'remark',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            null,
                            ['nullable => false'],
                            'Remark'
                    )
                    ->addColumn(
                            'status',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            1,
                            ['nullable' => false],
                            'status'
                    )
                    ->addColumn(
                            'created_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                            null,
                            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                            'Created At'
                    )
                    ->addColumn(
                            'updated_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                            null,
                            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                            'Updated At'
                    )
                    ->addIndex(
                            $setup->getIdxName('network_sponsor_tree', ['date']),
                            ['date']
                    )
                    ->addIndex(
                            $setup->getIdxName('network_sponsor_tree', ['customer_id']),
                            ['customer_id']
                    )
                    ->addIndex(
                            $setup->getIdxName('network_sponsor_tree', ['ref_id']),
                            ['ref_id']
                    )
                    ->addIndex(
                    $setup->getIdxName('network_sponsor_tree', ['from_id']),
                    ['from_id']
            );

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
