<?php

namespace Merchstack\Bonus\Observer;

use \Magento\Framework\Event\ObserverInterface;

class InvoiceBonusPayout implements ObserverInterface {

    const NEW = 0;
    const UPDATE_PV = 1;
    const PAY_BONUS = 2;
    const DIRECT_BONUS_RATE = 0.1;
    const DIRECT_BONUS_LEVEL = 3;

    protected $logger;
    protected $quoteFactory;
    protected $orderFactory;
    protected $dBonusFactory;
    protected $ewalletFactory;
    protected $dBonusRepo;
    protected $spTreeRepo;
    protected $dateTime;

    public function __construct(
            \Psr\Log\LoggerInterface $logger,
            \Magento\Quote\Model\QuoteFactory $quoteFactory,
            \Magento\Sales\Model\OrderFactory $orderFactory,
            \Merchstack\Bonus\Model\DirectBonusFactory $dBonusFactory,
            \Merchstack\Bonus\Api\DirectBonusRepositoryInterface $dBonusRepo,
            \Merchstack\NetworkTree\Api\NetworkSponsorTreeRepositoryInterface $spTreeRepo,
            \Merchstack\Ewallet\Model\EwalletFactory $ewalletFactory,
            \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTime) {
        $this->logger = $logger;
        $this->quoteFactory = $quoteFactory;
        $this->orderFactory = $orderFactory;
        $this->dBonusFactory = $dBonusFactory;
        $this->ewalletFactory = $ewalletFactory;
        $this->dBonusRepo = $dBonusRepo;
        $this->spTreeRepo = $spTreeRepo;
        $this->dateTime = $dateTime;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();

        $this->logger->debug('sales_order_invoice_save_after, Invoice Id: ' . $invoice->getId());
        
        $invoiceNo = $invoice->getData('increment_id');
        $bonusStatus = $invoice->getData('bonus_status');

        if ($bonusStatus == self::NEW) {
            // update pv for invoice
            $this->updatePv($invoice, $order);
            $bonusStatus = self::UPDATE_PV;
        }

        if ($bonusStatus == self::UPDATE_PV) {
            $dBonuses = $this->dBonusRepo->findByRefId($invoice->getId());
            if ($dBonuses->getSize() > 0) {
                // Already payout or no payout, skip payout
                $this->logger->error('Already performed bonus payout Invoice #' . $invoiceNo . ' skipping bonus payout');
            } else {
                // bonus payout
                $this->bonusPayout($invoice, $order);
            }

            $bonusStatus = self::PAY_BONUS;
        }

        if ($bonusStatus !== $invoice->getData('bonus_status')) {
            $invoice->setData('bonus_status', $bonusStatus);
            $invoice->save();
        }
    }

    private function updatePv($invoice, $order) {
        $this->logger->debug('Start updatePv, Invoice Id: ' . $invoice->getId());

        $totalPv = 0;

        $orderItems = $order->getAllItems();
        $invoiceItems = $invoice->getAllItems();

        foreach ($invoiceItems as $invoiceItem) {
            $iProductId = $invoiceItem->getData('product_id');
            foreach ($orderItems as $orderItem) {
                $oProductId = $orderItem->getData('product_id');
                $qty = $invoiceItem->getData('qty');

                if ($oProductId === $iProductId) {
                    $unitPv = $orderItem->getData('pv');
                    $invoiceItem->setData('pv', $unitPv);
                    $invoiceItem->setData('total_pv', $unitPv * $qty);
                    
                    $this->logger->debug('Order Item Unit Pv ' . $unitPv);
                    $this->logger->debug('Invoice Item Unit Pv ' . $unitPv);
                    $this->logger->debug('Invoice Item Total Pv updatePv ' . ($unitPv * $unitPv));

                    $invoiceItem->save();
                }
            }
            $totalPv += $invoiceItem->getData('total_pv');
        }
        $invoice->setData('total_pv', $totalPv);

        $this->logger->debug('Invoice Total Pv ' . $totalPv);
        $this->logger->debug('End updatePv, Invoice Id: ' . $invoice->getId());
    }

    private function bonusPayout($invoice, $order) {
        $totalPv = $invoice->getData('total_pv');
        if ($totalPv <= 0) {
            return;
        }

        $stmtDate = $this->dateTime->date()->format('Y-m-d');
        $fromId = $order->getData('customer_id');
        $invoiceNo = $invoice->getData('increment_id');

        // Add record to direct bonus

        $customerId = $fromId;
        $level = 1;
        $cont = true;

        $dBonuses = array();

        while ($cont) {
            if ($level > 1) {
                // get upline
                $upline = $this->spTreeRepo->getUpline($customerId);
                $customerId = $upline != null ? $upline->getId() : null;
            }

            if ($customerId !== null) {
                $dBonus = $this->dBonusFactory->create();
                $dBonus->setData('date', $stmtDate);
                $dBonus->setData('customer_id', $customerId);
                $dBonus->setData('from_id', $fromId);
                $dBonus->setData('pv', $totalPv);
                $dBonus->setData('level', $level++);
                $dBonus->setData('ref_id', $invoice->getId());
                $dBonus->setData('remark', 'Invoice # ' . $invoiceNo);
                $dBonus->setData('bonus', $this->getBonusPayout($totalPv));
                $dBonus->save();

                array_push($dBonuses, $dBonus);

                $cont = $level <= self::DIRECT_BONUS_LEVEL;
            } else {
                // can't find upline, stop payout
                $cont = false;
            }
        }

        // Save to ewallet
        foreach ($dBonuses as $dBonus) {
            $ewallet = $this->ewalletFactory->create();
            $ewallet->setData('owner_id', $dBonus->getData('customer_id'));
            $ewallet->setData('trx_type', 'DBI');
            $ewallet->setData('trx_date', $dBonus->getData('created_at'));
            $ewallet->setData('stmt_date', $dBonus->getData('date'));
            $ewallet->setData('amt_in', $dBonus->getData('bonus'));
            $ewallet->setData('remark', $dBonus->getData('remark'));
            $ewallet->setData('doneby', 'SYS');
            $ewallet->save();
        }
    }

    private function getBonusPayout($totalPv) {
        return self::DIRECT_BONUS_RATE * $totalPv;
    }

}
