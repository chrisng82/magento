<?php

namespace Merchstack\Bonus\Observer;

use \Magento\Framework\Event\ObserverInterface;

class RemoveQuoteItemPv implements ObserverInterface {

    public function __construct() {
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {    
        // Recalculate quote total pv
        $quote_item = $observer->getQuoteItem();
        $quote = $quote_item->getQuote();
      
        $total_pv = 0;
        
        $quote_items = $quote->getAllVisibleItems();
        foreach ($quote_items as $quote_item) {
            $total_pv += $quote_item->getData('total_pv');
        }
        
        $quote->setData('total_pv', $total_pv);
    }
}
