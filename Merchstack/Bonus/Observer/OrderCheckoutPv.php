<?php

namespace Merchstack\Bonus\Observer;

use \Magento\Framework\Event\ObserverInterface;

class OrderCheckoutPv implements ObserverInterface {    
    protected $logger;
    protected $quoteFactory;
    protected $orderFactory;

    public function __construct(
            \Psr\Log\LoggerInterface $logger,
            \Magento\Quote\Model\QuoteFactory $quoteFactory,
            \Magento\Sales\Model\OrderFactory $orderFactory) {
        $this->logger = $logger;
        $this->quoteFactory = $quoteFactory;
        $this->orderFactory = $orderFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        // Update pv for sales order
        $order = $observer->getEvent()->getOrder();
        $quote = $this->quoteFactory->create()->load($order->getQuoteId());

        $this->logger->debug('sales_order_save_after, Order Id: ' . $order->getId());
        
        $totalPv = 0;

        $quoteItems = $quote->getAllItems();
        $orderItems = $order->getAllItems();

        foreach ($orderItems as $orderItem) {
            $oProductId = $orderItem->getData('product_id');
            $qty = $orderItem->getData('qty_ordered');

            foreach ($quoteItems as $quoteItem) {
                $qProductId = $quoteItem->getData('product_id');
                if ($oProductId === $qProductId) {
                    $unitPv = $quoteItem->getData('pv');
                    $orderItem->setData('pv', $unitPv);
                    $orderItem->setData('total_pv', $unitPv * $qty);
                }
            }

            $totalPv += $orderItem->getData('total_pv');
        }

        $order->setData('total_pv', $totalPv);
        $order->save();
    }

}
