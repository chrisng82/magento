<?php

namespace Merchstack\Bonus\Observer;

use \Magento\Framework\Event\ObserverInterface;

class OrderInvoicePv implements ObserverInterface {
    protected $quoteFactory;
    protected $orderFactory;
    protected $dBonusFactory;

    public function __construct(
            \Magento\Quote\Model\QuoteFactory $quoteFactory,
            \Magento\Sales\Model\OrderFactory $orderFactory,
            \Merchstack\Bonus\Model\DirectBonusFactory $dBonusFactory) {
        $this->quoteFactory = $quoteFactory;
        $this->orderFactory = $orderFactory;
        $this->dBonusFactory = $dBonusFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        // Update pv for invoice
        $invoice = $observer->getEvent()->getInvoice();
        $order = $observer->getEvent()->getOrder();

        $totalPv = 0;

        $orderItems = $order->getAllItems();
        $invoiceItems = $invoice->getAllItems();

        foreach ($invoiceItems as $invoiceItem) {
            $iProductId = $invoiceItem->getData('product_id');
            foreach ($orderItems as $orderItem) {
                $oProductId = $orderItem->getData('product_id');
                $qty = $invoiceItem->getData('qty');

                if ($oProductId === $iProductId) {
                    $unitPv = $orderItem->getData('pv');
                    $invoiceItem->setData('pv', $unitPv);
                    $invoiceItem->setData('total_pv', $unitPv * $qty);
                }
            }

            $totalPv += $invoiceItem->getData('total_pv');
        }

        $invoice->setData('total_pv', $totalPv);
    }
}
