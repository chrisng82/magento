<?php

namespace Merchstack\Bonus\Block;

class Statement extends \Magento\Framework\View\Element\Template {

    protected $customerSession;
    protected $dBonusFactory;
    protected $dBonusRepo;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\Bonus\Model\DirectBonusFactory $dBonusFactory,
            \Merchstack\Bonus\Api\DirectBonusRepositoryInterface $dBonusRepo,
            \Magento\Customer\Model\CustomerFactory $customerFactory) {
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->dBonusRepo = $dBonusRepo;
        $this->dBonusFactory = $dBonusFactory;

        return parent::__construct($context);
    }

    public function getPeriods() {
        $customer = $this->customerSession->getCustomer();
        return $this->dBonusRepo->findPeriods($customer->getId());
    }

    public function getDirectBonuses($period) {
        $input = explode('-', $period);
        $from = $input[1] . '-' . $input[0] . '-1';
        $to = date("Y-m-t", strtotime($from));

        $customer = $this->customerSession->getCustomer();
        return $this->dBonusRepo->findByCustomerIdAndPeriod($customer->getId(), $from, $to);
    }

    public function getPeriodDisplay($period) {
        return date('F Y', mktime(0, 0, 0, $period['month'], 1, $period['year']));
    }

    public function getPeriodValue($period) {
        return $period['month'] . '-' . $period['year'];
    }
}
