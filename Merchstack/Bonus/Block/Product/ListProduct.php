<?php

namespace Merchstack\Bonus\Block\Product;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    public function getPV(\Magento\Catalog\Model\Product $product)
    {
        $pv = 0;
        if ($product->getCustomAttribute('pv') != null) {
            $pv = $product->getCustomAttribute('pv')->getValue();
        }
        return $pv;
    }
}
