<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * Product description block
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */

namespace Merchstack\Bonus\Block\Product\View;

use Magento\Catalog\Model\Product;

/**
 * @api
 * @since 100.0.2
 */
class PV extends \Magento\Framework\View\Element\Template {

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Framework\Registry $registry,
            array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct() {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    public function getPV() {
        $pv = 0;
        if ($this->getProduct()->getCustomAttribute('pv') != null) {
            $pv = $this->getProduct()->getCustomAttribute('pv')->getValue();
        }
        return $pv;
    }
}
