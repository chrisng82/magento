<?php

namespace Merchstack\Bonus\Model;

class DirectBonus extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'merchstack_bonus_directbonus';
    const TRACEKEY_SEPARATOR = '/';

    protected function _construct() {
        $this->_init('Merchstack\Bonus\Model\ResourceModel\DirectBonus');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];
        return $values;
    }
}
