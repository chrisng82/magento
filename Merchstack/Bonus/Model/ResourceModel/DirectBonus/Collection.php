<?php

namespace Merchstack\Bonus\Model\ResourceModel\DirectBonus;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct() {
        $this->_init('Merchstack\Bonus\Model\DirectBonus',
                'Merchstack\Bonus\Model\ResourceModel\DirectBonus');
    }

    public function getTotalBonus() {
        $total = 0;
        foreach ($this as $data) {
            $total += $data->getData('bonus');
        }
        return $total;
    }

}
