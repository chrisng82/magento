<?php

namespace Merchstack\Bonus\Model;

class DirectBonusRepository implements \Merchstack\Bonus\Api\DirectBonusRepositoryInterface {

    protected $resourceConnection;
    protected $dBonusFactory;
    protected $eavAttribute;

    public function __construct(
            \Magento\Framework\App\ResourceConnection $resourceConnection,
            \Merchstack\Bonus\Model\DirectBonusFactory $dBonusFactory,
            \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute) {
        $this->resourceConnection = $resourceConnection;
        $this->dBonusFactory = $dBonusFactory;
        $this->eavAttribute = $eavAttribute;
    }

    private function getMemberIdEavId() {
        return $this->eavAttribute->getIdByCode('customer', 'member_id');
    }

    public function findPeriods($customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = 'SELECT distinct MONTH(date) as month, YEAR(date) as year ' .
                'FROM direct_bonus ' .
                'WHERE customer_id = ' . $customerId . ' ' .
                'ORDER BY date DESC';
        return $connection->fetchAll($query);
    }

    public function findByCustomerIdAndPeriod($customerId, $from, $to) {
        $dBonus = $this->dBonusFactory->create();
        $collection = $dBonus->getCollection();
        $collection->getSelect()
                ->joinLeft(
                        ['customer' => $collection->getTable('customer_entity')],
                        'from_id = customer.entity_id',
                        ['customer.firstname', 'customer.lastname', 'customer.email']
                )
                ->joinLeft(
                        ['cv' => $collection->getTable('customer_entity_varchar')],
                        'from_id = cv.entity_id AND cv.attribute_id = ' . $this->getMemberIdEavId(),
                        ['cv.value as member_id']
                )
                ->where('customer_id = ?', $customerId)
                ->where('date >= ?', $from)
                ->where('date <= ?', $to);
        return $collection;
    }

    public function findByRefId($refId) {
        $dBonus = $this->dBonusFactory->create();
        $collection = $dBonus->getCollection()
                ->addFieldToFilter('ref_id', ['eq' => $refId]);
        return $collection;
    }

}
