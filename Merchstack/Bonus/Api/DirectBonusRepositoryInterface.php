<?php

namespace Merchstack\Bonus\Api;

interface DirectBonusRepositoryInterface {
    public function findPeriods($customerId);
    
    public function findByCustomerIdAndPeriod($customerId, $from, $to);
    
    public function findByRefId($refId);
}
