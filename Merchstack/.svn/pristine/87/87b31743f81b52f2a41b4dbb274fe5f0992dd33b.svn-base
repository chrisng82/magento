<?php

namespace Merchstack\Networktree\Block;

class NetworkTree extends \Magento\Framework\View\Element\Template {

    protected $customerSession;
    protected $spTreeFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\Networktree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory) {
        $this->customerSession = $customerSession;
        $this->spTreeFactory = $spTreeFactory;
        $this->customerFactory = $customerFactory;
        return parent::__construct($context);
    }

    public function findCurrent() {
        $customer = $this->customerSession->getCustomer();
        $spTree = $this->spTreeFactory->create();
        $spTree->load($customer->getId(), 'customer_id');

        return $spTree->getId() != null ? $spTree : null;
    }

    public function findById($memberId) {
        $customer = $this->customerFactory->create();

        $collection = $customer->getCollection();
        $collection = $collection->addAttributeToFilter('member_id', $memberId);

        if (count($collection) > 0) {
            $customer = $collection->getFirstItem();
            $spTree = $this->spTreeFactory->create();
            $spTree->load($customer->getId(), 'customer_id');

            return $spTree->getId() != null ? $spTree : null;
        } else {
            // no match found
            return null;
        }
    }

    public function getUpline($memberId = null) {
        if ($memberId != null) {
            $spTree = $this->findById($memberId);
        } else {
            $spTree = $this->findCurrent();
        }

        $upline = $this->customerFactory->create();
        $upline->load($spTree->getData('parent_id'));
        $upline->setData('level', $spTree->getData('level') - 1);
        
        return $upline->getId() != null ? $upline : null;
    }

    public function getDownlines($level) {
        $spTree = $this->getCurrent();

        $collection = $spTree->getCollection()->getDownlines(
                $spTree->getData('tracekey'), $spTree->getData('level') + $level);
        return $collection;
    }
    
    public function countDownlines($memberId) {
        $spTree = $this->findById($memberId);
        
        $collection = $spTree->getCollection();
        return $collection->countDownlines($spTree->getData('tracekey'));
    }
    
    public function getTree($level, $memberId = null) {
        if ($memberId != null) {
            $spTree = $this->findById($memberId);
        } else {
            $spTree = $this->findCurrent();
        }

        $collection = $spTree->getCollection()->getTree(
                $spTree->getData('tracekey'),
                $spTree->getData('level'),
                $spTree->getData('level') + $level);
        return $collection;
    }
}
