<?php

namespace Merchstack\NetworkTree\Observer;

use \Magento\Framework\Event\ObserverInterface;

class SaveNetworkTreeObserver implements ObserverInterface {

    protected $_custFactory;
    protected $_spTreeFactory;
    protected $_helper;

    public function __construct(
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\NetworkTree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Merchstack\NetworkTree\Helper\MemberId $helper) {
        $this->_spTreeFactory = $spTreeFactory;
        $this->_custFactory = $customerFactory;
        $this->_helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $evtCust = $observer->getEvent()->getCustomer();

        $cust = $this->_custFactory->create();
        $cust->load($evtCust->getId());

        // Generate referral code
        $custDM = $cust->getDataModel();
        $custDM->setCustomAttribute(
                'member_id', $this->_helper->generateMemberId($cust->getId()));
        $cust->updateData($custDM);
        $cust->save();

        // Find introducer via referral code
        $collection = $cust->getCollection();
        $collection = $collection->addAttributeToFilter('member_id', $cust->getData('introducer'));

        if (count($collection) > 0) {
            $introducerId = $collection->getFirstItem()->getId();
        } else {
            $nTree = $this->_spTreeFactory->create();
            $nCollection = $nTree->getCollection();
            
            if ($nCollection->getSize() > 0) {
                // Assign to 1st customer
                $introducerId = $nCollection->getFirstItem()->getData('customer_id');
            } else {
                // Assign to root
                $introducerId = 0;
            }
        }

        // Insert into network sponsor tree
        $model = $this->_spTreeFactory->create();
        $model->setData('customer_id', $cust->getId());
        $model->setData('parent_id', $introducerId);
        $model->setData('introducer_id', $introducerId);
        $model->save();

        $model->parseTree();
    }

}
