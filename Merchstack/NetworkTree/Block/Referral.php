<?php

namespace Merchstack\NetworkTree\Block;

class Referral extends \Magento\Framework\View\Element\Template {

    protected $customerSession;
    protected $spTreeFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\NetworkTree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory) {
        $this->customerSession = $customerSession;
        $this->spTreeFactory = $spTreeFactory;
        $this->customerFactory = $customerFactory;
        return parent::__construct($context);
    }

    public function getCustomer() {
        $customer = $this->customerSession->getCustomer();

        $cust = $this->customerFactory->create();
        $cust->load($customer->getId());

        return $customer;
    }

    public function getReferralCode() {
        $customer = $this->customerSession->getCustomer();

        $cust = $this->customerFactory->create();
        $cust->load($customer->getId());

        return $cust->getData('member_id');
    }

}
