<?php

namespace Merchstack\NetworkTree\Block\Adminhtml;

class NetworkTree extends \Magento\Framework\View\Element\Template {
    protected $spTreeRepo;

    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Merchstack\NetworkTree\Api\NetworkSponsorTreeRepositoryInterface $spTreeRepo) {
        $this->spTreeRepo = $spTreeRepo;
        return parent::__construct($context);
    }

    public function findByMemberId($memberId) {
        return $this->spTreeRepo->findByMemberId($memberId);
    }

    public function getUpline($customerId) {
        return $this->spTreeRepo->getUpline($customerId);
    }

    public function getDownlines($customerId, $level) {
        return $this->spTreeRepo->getDownlines($customerId, $level);
    }
    
    public function countDownlines($customerId) {
        return $this->spTreeRepo->countDownlines($customerId);
    }
}
