<?php

namespace Merchstack\NetworkTree\Block\Adminhtml;

class MemberId extends \Magento\Framework\View\Element\Template {
    protected $customerSession;
    protected $spTreeFactory;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\NetworkTree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory) {
        $this->customerSession = $customerSession;
        $this->spTreeFactory = $spTreeFactory;
        $this->customerFactory = $customerFactory;
        return parent::__construct($context);
    }

    public function countCustomersNoMemberId() {
        $customer = $this->customerFactory->create();
        $collection = $customer->getCollection()->addAttributeToFilter('member_id', ['null' => true]);
        return $collection->getSize();
    }
}
