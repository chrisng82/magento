<?php

namespace Merchstack\NetworkTree\Block\Adminhtml;

class NetworkTreeParser extends \Magento\Framework\View\Element\Template {
    protected $customerSession;
    protected $spTreeRepo;

    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Merchstack\NetworkTree\Api\NetworkSponsorTreeRepositoryInterface $spTreeRepo) {
        $this->spTreeRepo = $spTreeRepo;
        return parent::__construct($context);
    }

    public function countUnparsedCustomers() {
        return $this->spTreeRepo->findUnparsed()->getSize();
    }
}
