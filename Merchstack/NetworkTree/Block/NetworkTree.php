<?php

namespace Merchstack\NetworkTree\Block;

class NetworkTree extends \Magento\Framework\View\Element\Template {
    protected $customerSession;
    protected $spTreeRepo;


    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\NetworkTree\Api\NetworkSponsorTreeRepositoryInterface $spTreeRepo) {
        $this->customerSession = $customerSession;
        $this->spTreeRepo = $spTreeRepo;
        return parent::__construct($context);
    }

    public function findCurrent() {
        $customer = $this->customerSession->getCustomer();
        return $this->spTreeRepo->findByCustomerId($customer->getId());
    }

    public function getUpline() {
        $customer = $this->customerSession->getCustomer();
        return $this->spTreeRepo->getUpline($customer->getId());
    }

    public function getDownlines($level) {
        $customer = $this->customerSession->getCustomer();
        return $this->spTreeRepo->getDownlines($customer->getId(), $level);
    }
}
