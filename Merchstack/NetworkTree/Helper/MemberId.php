<?php

namespace Merchstack\NetworkTree\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class MemberId extends AbstractHelper {
    public function generateMemberId($id) {
         return '1' . str_pad($id, 8, '0', STR_PAD_LEFT);
    }
}