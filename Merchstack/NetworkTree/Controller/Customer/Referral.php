<?php

namespace Merchstack\NetworkTree\Controller\Customer;

class Referral extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_customerSession;
    protected $_customerRepo;
    protected $_spTreeFactory;
    protected $_customerFactory;
    protected $_urlInterface;
    
    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\NetworkTree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\UrlInterface $urlInterface) {
        $this->_pageFactory = $pageFactory;
        $this->_customerSession = $customerSession;
        $this->_spTreeFactory = $spTreeFactory;
        $this->_customerFactory = $customerFactory;
        $this->_urlInterface = $urlInterface;
        return parent::__construct($context);
    }

    public function execute() {
        if (!$this->_customerSession->isLoggedIn()) {
            $this->_customerSession->setAfterAuthUrl($this->_urlInterface->getCurrentUrl());
            $this->_customerSession->authenticate();
        } else {
            return $this->_pageFactory->create();
        }
    }
}
