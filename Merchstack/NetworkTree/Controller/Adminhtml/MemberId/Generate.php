<?php

namespace Merchstack\NetworkTree\Controller\Adminhtml\MemberId;

class Generate extends \Magento\Backend\App\Action {
    protected $customerSession;
    protected $customerFactory;
    protected $urlInterface;
    protected $pageFactory;
    protected $memberId;

    public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\NetworkTree\Helper\MemberId $memberId,
            \Magento\Framework\UrlInterface $urlInterface) {
        $this->pageFactory = $pageFactory;
        $this->customerFactory = $customerFactory;
        $this->urlInterface = $urlInterface;
        $this->memberId = $memberId;
        return parent::__construct($context);
    }

    public function execute() {
        $cust = $this->customerFactory->create();
        $customers = $cust->getCollection()->addAttributeToFilter('member_id', ['null' => true]);
        
        foreach ($customers as $customer) {
            $custDM = $customer->getDataModel();
            $custDM->setCustomAttribute(
                    'member_id', $this->memberId->generateMemberId($customer->getId()));
            $customer->updateData($custDM);
            $customer->save();
        }
        
        $this->messageManager->addSuccess(__('Member Ids generated.'));
        $this->_redirect('networktree/memberid/missing');
    }
}

?>