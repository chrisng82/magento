<?php

namespace Merchstack\NetworkTree\Controller\Adminhtml\MemberId;

class Missing extends \Magento\Backend\App\Action {
    protected $pageFactory;

    public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute() {
        $resultPage = $this->pageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Generate Member Id'));
        return $resultPage;
    }
}
?>