<?php

namespace Merchstack\NetworkTree\Controller\Adminhtml\Customer;

class Parse extends \Magento\Backend\App\Action {

    protected $customerSession;
    protected $customerFactory;
    protected $spTreeFactory;
    protected $spTreeRepo;
    protected $urlInterface;
    protected $pageFactory;
    protected $memberId;

    public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Merchstack\NetworkTree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Merchstack\NetworkTree\Api\NetworkSponsorTreeRepositoryInterface $spTreeRepo,
            \Merchstack\NetworkTree\Helper\MemberId $memberId,
            \Magento\Framework\UrlInterface $urlInterface) {
        $this->pageFactory = $pageFactory;
        $this->customerFactory = $customerFactory;
        $this->spTreeFactory = $spTreeFactory;
        $this->spTreeRepo = $spTreeRepo;
        $this->urlInterface = $urlInterface;
        $this->memberId = $memberId;
        return parent::__construct($context);
    }

    public function execute() {
        $this->spTreeRepo->parseTree();

        $this->messageManager->addSuccess(__('Member Network Tree parsed.'));
        $this->_redirect('networktree/customer/missing');
    }

}
?>