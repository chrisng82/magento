<?php

namespace Merchstack\NetworkTree\Api;

interface NetworkSponsorTreeRepositoryInterface {

    public function findByCustomerId($customerId);

    public function findByMemberId($memberId);

    public function getUpline($customerId);

    public function getDownlines($customerId, $level);

    public function getRoot();

    public function countDownlines($customerId);

    public function findUnparsed();

    public function parseTree();
}
