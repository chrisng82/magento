<?php

namespace Merchstack\NetworkTree\Model;

class NetworkSponsorTreeRepository implements \Merchstack\NetworkTree\Api\NetworkSponsorTreeRepositoryInterface {

    protected $spTreeFactory;
    protected $customerFactory;
    protected $eavAttribute;

    public function __construct(
            \Merchstack\NetworkTree\Model\NetworkSponsorTreeFactory $spTreeFactory,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute) {
        $this->spTreeFactory = $spTreeFactory;
        $this->customerFactory = $customerFactory;
        $this->eavAttribute = $eavAttribute;
    }

    private function getMemberIdEavId() {
        return $this->eavAttribute->getIdByCode('customer', 'member_id');
    }

    public function findByCustomerId($customerId) {
        $spTree = $this->spTreeFactory->create();
        $spTree->load($customerId, 'customer_id');
        return $spTree;
    }

    public function findByMemberId($memberId) {
        $spTree = $this->spTreeFactory->create();
        $collection = $spTree->getCollection();
        $collection->getSelect()
                ->joinLeft(
                        ['customer' => $collection->getTable('customer_entity')],
                        'customer_id = customer.entity_id',
                        ['customer.firstname', 'customer.lastname', 'customer.email']
                )
                ->joinLeft(
                        ['cv' => $collection->getTable('customer_entity_varchar')],
                        'customer_id = cv.entity_id AND cv.attribute_id = ' . $this->getMemberIdEavId(),
                        ['cv.value as member_id']
                )
                ->where('cv.value = ?', $memberId)
                ->order('tracekey asc');

        return $collection->getSize() > 0 ? $collection->getFirstItem() : null;
    }

    public function getUpline($customerId) {
        $spTree = $this->spTreeFactory->create();
        $spTree->load($customerId, 'customer_id');

        $parentId = $spTree->getData('parent_id');

        $collection = $spTree->getCollection();
        $collection->getSelect()
                ->joinLeft(
                        ['customer' => $collection->getTable('customer_entity')],
                        'customer_id = customer.entity_id',
                        ['customer.firstname', 'customer.lastname', 'customer.email']
                )
                ->joinLeft(
                        ['cv' => $collection->getTable('customer_entity_varchar')],
                        'customer_id = cv.entity_id AND cv.attribute_id = ' . $this->getMemberIdEavId(),
                        ['cv.value as member_id']
                )
                ->where('customer_id = ?', $parentId)
                ->order('tracekey asc');

        return $collection->getSize() > 0 ? $collection->getFirstItem() : null;
    }

    public function getDownlines($customerId, $level) {
        $spTree = $this->spTreeFactory->create();
        $spTree->load($customerId, 'customer_id');

        $tracekey = $spTree->getData('tracekey');
        $minLevel = $spTree->getData('level') + 1;
        $maxLevel = $spTree->getData('level') + $level;

        $collection = $spTree->getCollection();
        $collection->getSelect()
                ->joinLeft(
                        ['customer' => $collection->getTable('customer_entity')],
                        'customer_id = customer.entity_id',
                        ['customer.firstname', 'customer.lastname', 'customer.email']
                )
                ->joinLeft(
                        ['cv' => $collection->getTable('customer_entity_varchar')],
                        'customer_id = cv.entity_id AND cv.attribute_id = ' . $this->getMemberIdEavId(),
                        ['cv.value as member_id']
                )
                ->where('tracekey like ?', $tracekey . '/%')
                ->where('level >= ?', $minLevel)
                ->where('level <= ?', $maxLevel)
                ->order('tracekey asc');

        return $collection;
    }

    public function countDownlines($customerId) {
        $spTree = $this->spTreeFactory->create();
        $spTree->load($customerId, 'customer_id');

        $tracekey = $spTree->getData('tracekey');

        $collection = $spTree->getCollection();
        $collection->getSelect()->where('tracekey like ?', $tracekey . '/%');
        return $collection->getSize();
    }

    public function findUnparsed() {
        $spTree = $this->spTreeFactory->create();
        $collection = $spTree->getCollection();

        $collection->getSelect()
                ->joinRight(
                        ['customer' => $collection->getTable('customer_entity')],
                        'customer_id = customer.entity_id',
                        ['customer.entity_id']
                )
                ->where('id is null');
        return $collection;
    }

    public function getRoot() {
        $spTree = $this->spTreeFactory->create();
        $collection = $spTree->getCollection();
        $collection->getSelect()->where('level = 1');
        return $collection->getSize() > 0 ? $collection->getFirstItem() : null;
    }

    public function parseTree() {
        $retry = true;
        $uCounter = 0;

        while ($retry) {
            $uCustomers = $this->findUnparsed();
            if ($uCustomers->getSize() == 0) {
                return;
            }

            $nCounter = 0;
            foreach ($uCustomers as $uCustomer) {
                // Find sponsor
                $customerId = $uCustomer->getData('entity_id');
                $customer = $this->customerFactory->create();
                $customer->load($customerId);

                $introducer = $customer->getData('introducer');
                $parent = $introducer != null ? $this->findByMemberId($introducer) : null;

                if ($introducer == null || $parent == null) {
                    $this->addToRoot($customer);
                } else {
                    $introducerId = $parent->getData('customer_id');
                    $tracekey = $parent->getData('tracekey') . NetworkSponsorTree::TRACEKEY_SEPARATOR;
                    $level = $parent->getData('level');

                    $model = $this->spTreeFactory->create();
                    $model->setData('customer_id', $customerId);
                    $model->setData('tracekey', $tracekey . $customerId);
                    $model->setData('level', $level + 1);
                    $model->setData('parent_id', $introducerId);
                    $model->setData('introducer_id', $introducerId);
                    $model->save();
                }
            }

            $retry = $uCounter != $nCounter;
            $uCounter = $nCounter;
        }
    }

    private function addToRoot($customer) {
        $root = $this->getRoot();
        if ($root == null) {
            // No root, assign as root
            $introducerId = 0;
            $tracekey = '';
            $level = 0;
        } else {
            // assign under root
            $introducerId = $root->getData('customer_id');
            $tracekey = $root->getData('tracekey') . NetworkSponsorTree::TRACEKEY_SEPARATOR;
            $level = $root->getData('level');
        }

        $model = $this->spTreeFactory->create();
        $model->setData('customer_id', $customer->getId());
        $model->setData('tracekey', $tracekey . $customer->getId());
        $model->setData('level', $level + 1);
        $model->setData('parent_id', $introducerId);
        $model->setData('introducer_id', $introducerId);
        $model->save();
    }

}
