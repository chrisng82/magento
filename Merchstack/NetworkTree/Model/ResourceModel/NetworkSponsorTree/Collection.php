<?php

namespace Merchstack\NetworkTree\Model\ResourceModel\NetworkSponsorTree;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct() {
        $this->_init('Merchstack\NetworkTree\Model\NetworkSponsorTree',
                'Merchstack\NetworkTree\Model\ResourceModel\NetworkSponsorTree');
    }
}
