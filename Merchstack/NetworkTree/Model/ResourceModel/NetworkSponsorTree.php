<?php

namespace Merchstack\NetworkTree\Model\ResourceModel;

class NetworkSponsorTree extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('network_sponsor_tree', 'id');
    }
}
