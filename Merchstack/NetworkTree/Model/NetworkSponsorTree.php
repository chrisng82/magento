<?php

namespace Merchstack\NetworkTree\Model;

class NetworkSponsorTree extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'merchstack_networktree_networksponsortree';
    const TRACEKEY_SEPARATOR = '/';

    protected function _construct() {
        $this->_init('Merchstack\NetworkTree\Model\ResourceModel\NetworkSponsorTree');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];
        return $values;
    }
}
