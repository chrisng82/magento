<?php

namespace Merchstack\NetworkTree\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\Customer;

class InstallData implements InstallDataInterface {
    protected $_eavSetupFactory;
    protected $_eavConfig;
    protected $_attrResource;

    public function __construct(
            \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
            \Magento\Eav\Model\Config $eavConfig,
            \Magento\Customer\Model\ResourceModel\Attribute $attributeResource) {
        $this->_eavConfig = $eavConfig;
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_attrResource = $attributeResource;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->removeAttribute(Customer::ENTITY, 'introducer');

        $attrSetId = $eavSetup->getDefaultAttributeSetId(Customer::ENTITY);
        $attrGrpId = $eavSetup->getDefaultAttributeGroupId(Customer::ENTITY);
        
        $eavSetup->addAttribute(Customer::ENTITY, 'introducer', [
            'type' => 'varchar',
            'label' => 'Introducer',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 990,
            'position' => 990,
            'system' => 0
        ]);
        
        $eavSetup->addAttribute(Customer::ENTITY, 'member_id', [
            'type' => 'varchar',
            'label' => 'Referral Code',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => false,
            'sort_order' => 991,
            'position' => 991,
            'system' => 0
        ]);
        
        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'introducer');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);
        
        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'member_id');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);
    }
}
