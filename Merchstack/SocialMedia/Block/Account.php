<?php

namespace Merchstack\SocialMedia\Block;

class Account extends \Magento\Framework\View\Element\Template {
    protected $customerSession;
    protected $customerFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\Customer\Model\CustomerFactory $customerFactory) {
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        return parent::__construct($context);
    }
    
    public function getCustomer() {
        $customer = $this->customerSession->getCustomer();

        $cust = $this->customerFactory->create();
        $cust->load($customer->getId());
    
        return $cust;
    }
}
