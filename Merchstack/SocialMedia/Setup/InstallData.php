<?php

namespace Merchstack\SocialMedia\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\Customer;

class InstallData implements InstallDataInterface {

    protected $_eavSetupFactory;
    protected $_eavConfig;
    protected $_attrResource;

    public function __construct(
            \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
            \Magento\Eav\Model\Config $eavConfig,
            \Magento\Customer\Model\ResourceModel\Attribute $attributeResource) {
        $this->_eavConfig = $eavConfig;
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_attrResource = $attributeResource;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);

        $attrSetId = $eavSetup->getDefaultAttributeSetId(Customer::ENTITY);
        $attrGrpId = $eavSetup->getDefaultAttributeGroupId(Customer::ENTITY);

        $eavSetup->addAttribute(Customer::ENTITY, 'instagram1', [
            'type' => 'varchar',
            'label' => 'Instagram 1',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1000,
            'position' => 1000,
            'system' => 0
        ]);

        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'instagram1');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);

        $eavSetup->addAttribute(Customer::ENTITY, 'instagram2', [
            'type' => 'varchar',
            'label' => 'Instagram 2',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1001,
            'position' => 1001,
            'system' => 0
        ]);

        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'instagram2');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);

        $eavSetup->addAttribute(Customer::ENTITY, 'instagram3', [
            'type' => 'varchar',
            'label' => 'Instagram 3',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1002,
            'position' => 1002,
            'system' => 0
        ]);

        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'instagram3');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);

        $eavSetup->addAttribute(Customer::ENTITY, 'twitter1', [
            'type' => 'varchar',
            'label' => 'Twitter 1',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1003,
            'position' => 1003,
            'system' => 0
        ]);

        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'twitter1');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);

        $eavSetup->addAttribute(Customer::ENTITY, 'twitter2', [
            'type' => 'varchar',
            'label' => 'Twitter 2',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1004,
            'position' => 1004,
            'system' => 0
        ]);

        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'twitter2');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);

        $eavSetup->addAttribute(Customer::ENTITY, 'twitter3', [
            'type' => 'varchar',
            'label' => 'Twitter 3',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1005,
            'position' => 1005,
            'system' => 0
        ]);

        $attr = $this->_eavConfig->getAttribute(Customer::ENTITY, 'twitter3');
        $attr->setData('attribute_set_id', $attrSetId);
        $attr->setData('attribute_group_id', $attrGrpId);
        $attr->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_edit'
        ]);
        $this->_attrResource->save($attr);
    }

}
