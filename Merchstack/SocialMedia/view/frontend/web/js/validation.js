define([
    'jquery'
], function ($) {
    return function (validator) {
        $.validator.addMethod(
                'validate-instagram',
                function (value) {
                    var p = '^\s*(http\:\/\/)?instagram\.com\/[a-z\A-Z\d\-]{1,255}\s*$';
                    if (value === '') {
                        return true;
                    } else {
                        return value.match(p);
                    }
                },
                $.mage.__('Please enter a valid Instagram. Value must be http://instagram.com/user')
                );
        $.validator.addMethod(
                'validate-twitter',
                function (value) {
                    var p = '^\s*(http\:\/\/)?twitter\.com\/[a-z\A-Z\d\-]{1,255}\s*$';
                    if (value === '') {
                        return true;
                    } else {
                        return value.match(p);
                    }
                },
                $.mage.__('Please enter a valid Twitter. Value must be http://twitter.com/user')
                );
        return validator;
    }
});