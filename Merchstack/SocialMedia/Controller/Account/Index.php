<?php

namespace Merchstack\SocialMedia\Controller\Account;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_customerSession;
    protected $_customerRepo;
    protected $_customerFactory;
    protected $_urlInterface;
    
    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\UrlInterface $urlInterface) {
        $this->_pageFactory = $pageFactory;
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        $this->_urlInterface = $urlInterface;
        return parent::__construct($context);
    }

    public function execute() {
        if (!$this->_customerSession->isLoggedIn()) {
            $this->_customerSession->setAfterAuthUrl($this->_urlInterface->getCurrentUrl());
            $this->_customerSession->authenticate();
        } else {
            return $this->_pageFactory->create();
        }
    }
}
