<?php

namespace Merchstack\SocialMedia\Controller\Account;

class Edit extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_customerSession;
    protected $_customerRepo;
    protected $_customerFactory;
    protected $_urlInterface;

    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\UrlInterface $urlInterface) {
        $this->_pageFactory = $pageFactory;
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        $this->_urlInterface = $urlInterface;
        return parent::__construct($context);
    }

    public function execute() {
        if (!$this->_customerSession->isLoggedIn()) {
            $this->_customerSession->setAfterAuthUrl($this->_urlInterface->getCurrentUrl());
            $this->_customerSession->authenticate();
        } else {
            $this->messageManager->addSuccess(__('Social media information saved.'));

            $customer = $this->_customerSession->getCustomer();

            $cust = $this->_customerFactory->create();
            $cust->load($customer->getId());

            $custDM = $cust->getDataModel();
            $custDM->setCustomAttribute('instagram1', $this->getRequest()->getParam('instagram1'));
            $custDM->setCustomAttribute('instagram2', $this->getRequest()->getParam('instagram2'));
            $custDM->setCustomAttribute('instagram3', $this->getRequest()->getParam('instagram3'));
            $custDM->setCustomAttribute('twitter1', $this->getRequest()->getParam('twitter1'));
            $custDM->setCustomAttribute('twitter2', $this->getRequest()->getParam('twitter2'));
            $custDM->setCustomAttribute('twitter3', $this->getRequest()->getParam('twitter3'));

            $cust->updateData($custDM);
            $cust->save();

            $this->_redirect('socialmedia/account/index');
        }
    }

}
