<?php
namespace Merchstack\SocialShare\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	protected $_shareTrackingFactory;

	public function __construct(
			\Magento\Framework\App\Action\Context $context,
			\Magento\Framework\View\Result\PageFactory $pageFactory,
			\Merchstack\SocialShare\Model\SocialShareTrackingFactory $shareTrackingFactory
			)
	{
		$this->_pageFactory = $pageFactory;
		$this->_shareTrackingFactory = $shareTrackingFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		$shareTracking = $this->_shareTrackingFactory->create();
		$collection = $shareTracking->getCollection();
		foreach($collection as $item){
			echo "<pre>";
			print_r($item->getData());
			echo "</pre>";
		}
		exit();
		return $this->_pageFactory->create();
	}
}