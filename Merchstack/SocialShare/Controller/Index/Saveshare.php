<?php

namespace Merchstack\SocialShare\Controller\Index;

use \Merchstack\RedeemEwallet\Model\RedeemEwalletType;
use \Merchstack\SocialShare\Model\SocialShareTrackingType;

class Saveshare extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $postFactory;
    protected $_shareTrackingFactory;
    protected $_customerSession;
    protected $_customerFactory;
    protected $logger;
    protected $redeemEwalletFactory;

    public function __construct(
            \Psr\Log\LoggerInterface $logger,
            \Magefan\Blog\Model\PostFactory $postFactory,
            \Magento\Framework\App\Action\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Merchstack\SocialShare\Model\SocialShareTrackingFactory $shareTrackingFactory,
            \Merchstack\RedeemEwallet\Model\RedeemEwalletFactory $redeemEwalletFactory) {
        $this->logger = $logger;
        $this->_pageFactory = $pageFactory;
        $this->postFactory = $postFactory;
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        $this->_shareTrackingFactory = $shareTrackingFactory;
        $this->redeemEwalletFactory = $redeemEwalletFactory;
        return parent::__construct($context);
    }

    public function execute() {
        if (!$this->_customerSession->isLoggedIn()) {
            // Do nothing
            $this->logger->info('no login');
        } else {

            // Get the customer id who logged in.
            $customer = $this->_customerSession->getCustomer();
            $cust = $this->_customerFactory->create();
            $cust->load($customer->getId());

            $data = $this->getRequest()->getPost();
            $date = date('Y-m-d h:i:sa');

            $sharerId = $customer->getId();
            $authorId = $data['post_author_id'];
            $postId = $data['post_id'];
            $sharedTo = $data['shared_to'];

            $sharerTrack = $this->trackSharer($sharerId, $authorId, $postId, $sharedTo, $date);
            $this->redeemEwallet($sharerTrack);

            if ($authorId > 0) {
                $authorTrack = $this->trackAuthor($sharerId, $authorId, $postId, $sharedTo, $date);
                $this->redeemEwallet($authorTrack);
            }
        }
    }

    protected function trackSharer($sharerId, $authorId, $postId, $sharedTo, $date) {
        $shareTrack = $this->_shareTrackingFactory->create();
        $shareTrack->setData('customer_id', $sharerId);
        $shareTrack->setData('shared_by_id', $sharerId);
        $shareTrack->setData('post_author_id', $authorId);
        $shareTrack->setData('post_id', $postId);
        $shareTrack->setData('shared_to', $sharedTo);
        $shareTrack->setData('type', SocialShareTrackingType::SHARING_CONTENT);
        $shareTrack->setData('pv2', 1);
        $shareTrack->setData('posted_date', '1990-01-01');
        $shareTrack->setData('shared_date', $date);
        $shareTrack->setData('created_at', $date);
        $shareTrack->setData('updated_at', $date);
        $shareTrack->save();

        return $shareTrack;
    }

    protected function trackAuthor($sharerId, $authorId, $postId, $sharedTo, $date) {
        $authorTrack = $this->_shareTrackingFactory->create();
        $authorTrack->setData('customer_id', $authorId);
        $authorTrack->setData('shared_by_id', $sharerId);
        $authorTrack->setData('post_author_id', $authorId);
        $authorTrack->setData('post_id', $postId);
        $authorTrack->setData('shared_to', $sharedTo);
        $authorTrack->setData('type', SocialShareTrackingType::CONTENT_SHARED);
        $authorTrack->setData('pv2', 1);
        $authorTrack->setData('posted_date', '1990-01-01');
        $authorTrack->setData('shared_date', $date);
        $authorTrack->setData('created_at', $date);
        $authorTrack->setData('updated_at', $date);
        $authorTrack->save();

        return $authorTrack;
    }

    protected function redeemEwallet($shareTrack) {
        if ($shareTrack->getData('type') == SocialShareTrackingType::CONTENT_SHARED) {
            $type = RedeemEwalletType::CONTENT_SHARED_IN;
            $remark = 'Reward for content shared';
        } else {
            $type = RedeemEwalletType::SHARED_CONTENT_IN;
            $remark = 'Reward for sharing content';
        }

        $date = $shareTrack->getData('shared_date');

        $ewallet = $this->redeemEwalletFactory->create();
        $ewallet->setData('owner_id', $shareTrack->getData('customer_id'));
        $ewallet->setData('trx_type', $type);
        $ewallet->setData('ref_id', $shareTrack->getId());
        $ewallet->setData('trx_date', $date);
        $ewallet->setData('stmt_date', date("Y-m-d", strtotime($date)));
        $ewallet->setData('amt_in', $shareTrack->getData('pv2'));
        $ewallet->setData('remark', $remark);
        $ewallet->setData('doneby', 'SYS');
        $ewallet->save();
    }

}
