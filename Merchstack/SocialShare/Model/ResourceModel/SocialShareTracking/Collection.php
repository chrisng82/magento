<?php
namespace Merchstack\SocialShare\Model\ResourceModel\SocialShareTracking;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Merchstack\SocialShare\Model\SocialShareTracking', 
				'Merchstack\SocialShare\Model\ResourceModel\SocialShareTracking');
	}

}