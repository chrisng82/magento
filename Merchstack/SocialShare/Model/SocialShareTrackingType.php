<?php

namespace Merchstack\SocialShare\Model;

class SocialShareTrackingType {

    const SHARING_CONTENT = "SC";
    const CONTENT_SHARED = "CS";

}
