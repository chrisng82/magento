<?php

namespace Merchstack\SocialShare\Model;

class SocialShareTracking extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'merchstack_socialshare_socialsharetracking';

	protected $_cacheTag = 'merchstack_socialshare_socialsharetracking';

	protected $_eventPrefix = 'merchstack_socialshare_socialsharetracking';

	protected function _construct()
	{
		$this->_init('Merchstack\SocialShare\Model\ResourceModel\SocialShareTracking');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}