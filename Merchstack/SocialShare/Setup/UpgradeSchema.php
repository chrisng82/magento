<?php

namespace Merchstack\SocialShare\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            // Add post id column
            $setup->getConnection()->addColumn(
                    $setup->getTable('social_share_tracking'),
                    'post_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => '11',
                        'default' => 0,
                        'nullable' => false,
                        'comment' => 'Blog post id',
                        'after' => 'post_author_id'
                    ]
            );

            // Drop post url column
            $setup->getConnection()->dropColumn($setup->getTable('social_share_tracking'), 'post_url');

            // Change pv2 column data type
            $setup->getConnection()->changeColumn(
                    $setup->getTable('social_share_tracking'),
                    'pv2',
                    'pv2',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,4',
                        'default' => 0,
                        'nullable' => false,
                        'comment' => 'PV2'
                    ]
            );
        }

        $installer->endSetup();
    }

}
