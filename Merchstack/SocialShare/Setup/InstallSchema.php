<?php

namespace Merchstack\SocialShare\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        /**
         * Create table 'social_share_tracking'
         */
        $table = $setup->getConnection()
                ->newTable($setup->getTable('social_share_tracking'))
                ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'ID'
                )
                ->addColumn(
                        'customer_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        10,
                        ['nullable' => false],
                        'Customer Id'
                )                
                ->addColumn(
                		'post_url',
                		\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                		null,
                		['nullable => false'],
                		'URL'
                )
                ->addColumn(
                		'post_author_id',
                		\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                		10,
                		['nullable' => false],
                		'Post Author Id'
                )
                ->addColumn(
                		'post_tags',
                		\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                		null,
                		['nullable' => false],
                		'Post Tags'
                )                
                ->addColumn(
                        'shared_by_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        10,
                        ['nullable' => false],
                        'Shared By'
                )
                ->addColumn(
                		'shared_to',
                		\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                		null,
                		['nullable' => false],
                		'Shared To'
                )
                ->addColumn(
                		'shared_date',
                		\Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                		null,
                		[ 'nullable' => false ],
                		'Shared Date'
                )
                ->addColumn(
                		'posted_date',
                		\Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                		null,
                		[ 'nullable' => false ],
                		'Posted Date'
                )
                ->addColumn(
                		'type',
                		\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                		null,
                		['nullable' => false],
                		'Type'
                )
                ->addColumn(
                		'pv2',
                		\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                		10,
                		['nullable' => false],
                		'PV2'
                )                
                ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Created At'
                )
                ->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                        'Updated At'
                )
                ->addIndex(
                        $setup->getIdxName('social_share_tracking', ['customer_id']),
                        ['customer_id']
                 )
                ->addIndex(
                 		$setup->getIdxName('social_share_tracking', ['post_author_id']),
                 		['post_author_id']
               	)
                ->addIndex(
                        $setup->getIdxName('social_share_tracking', ['shared_by_id']),
                        ['shared_by_id']
                 );
        
		$setup->getConnection()->createTable($table);

    }
}
