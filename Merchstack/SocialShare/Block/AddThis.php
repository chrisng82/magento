<?php

namespace Merchstack\SocialShare\Block;

class AddThis extends \Magento\Framework\View\Element\Template {

    protected $ssTrackingFactory;
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
            \Magento\Customer\Model\Session $customerSession,
            \Merchstack\SocialShare\Model\SocialShareTrackingFactory $ssTrackingFactory,
            \Magento\Framework\Registry $coreRegistry,
            \Magento\Customer\Model\CustomerFactory $customerFactory) {
        $this->_customerSession = $customerSession;
        $this->ssTrackingFactory = $ssTrackingFactory;
        $this->customerFactory = $customerFactory;
        $this->_coreRegistry = $coreRegistry;
        return parent::__construct($context);
    }

    /**
     * Retrieve post instance
     *
     * @return \Magefan\Blog\Model\Post
     */
    public function getPost() {
        return $this->_coreRegistry->registry('current_blog_post');
    }

    /**
     * Retrieve customer login in
     *
     * @return customer id
     */
    public function getCustomerID() {
        if (!$this->_customerSession->isLoggedIn()) {
            // Do nothing
            return 0;
        } else {
            $customer = $this->_customerSession->getCustomer();
            return $customer->getId();
        }
    }

}
