<?php
namespace MerchBlog\Templates\Block\Adminhtml\Template\Edit;

/**
 * Adminhtml blog post edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    protected $_wysiwygConfig;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('template_form');
        $this->setTitle(__('Template Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var MerchBlog\Templates\Model\Template $model */
        $model = $this->_coreRegistry->registry('merchblog_template');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 
                        'enctype' => 'multipart/form-data', 
                        'action' => $this->getData('action'), 
                        'method' => 'post']]
        );

        $form->setHtmlIdPrefix('template_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'title',
            'text',
            ['name' => 'title', 'label' => __('Template Title'), 'title' => __('Template Title'), 'required' => true]
        );

        $fieldset->addField(
            'summary',
            'text',
            [
                'name' => 'summary',
                'label' => __('Summary'),
                'title' => __('Summary'),
                'required' => true,
                //'class' => 'validate-xml-identifier'
            ]
        );
        
        $fieldset->addField(
            'description',
            'text',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => true,
                //'class' => 'validate-xml-identifier'
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );
        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }
        
        $fieldset->addField(
            'font_family',
            'select',
            [
                'label' => __('Font Family'),
                'title' => __('Font Family'),
                'name' => 'font_family',
                'options' => [
                    '' => ' ', 
                    'Arial' => 'Arial', 
                    'Book Antiqua' => 'Book Antiqua', 
                    'Helvetica' => 'Helvetica',
                    'Tahoma' => 'Tahoma',
                    'Times New Roman' => 'Times New Roman'
                    ]
            ]
        );
        
        $fieldset->addField(
            'header',
            'editor',
            [
                'name' => 'header',
                'label' => __('Header'),
                'title' => __('Header'),
                'style' => 'height:5em',
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );
        
        $fieldset->addField(
            'footer',
            'editor',
            [
                'name' => 'footer',
                'label' => __('Footer'),
                'title' => __('Footer'),
                'style' => 'height:5em',
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );

        for ($i=1; $i < 11; $i++) { 
            $fieldset->addField(
                'section_'.$i.'_title',
                'text',
                [
                    'name' => 'section_'.$i.'_title',
                    'label' => __('Section '.$i.' Title'),
                    'title' => __('Section '.$i.' Title'),
                ]
            );
            $fieldset->addField(
                'section_'.$i.'_description',
                'text',
                [
                    'name' => 'section_'.$i.'_description',
                    'label' => __('Section '.$i.' Description'),
                    'title' => __('Section '.$i.' Description'),
                ]
            );
        }

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}