<?php
namespace MerchBlog\Templates\Api\Data;

interface TemplateInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID       = 'id';
    const AUTHOR_ID     = 'author_id';
    const TITLE         = 'title';
    const SUMMARY       = 'summary';
    const DESCRIPTION   = 'description';
    const CONTENT       = 'content';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const IS_ACTIVE     = 'is_active';
    const FONT_FAMILY   = 'font_family';
    const BG_COLOR      = 'bg_color';
    const HEADER        = 'header';
    const FOOTER        = 'footer';
    const SECTION_1_TITLE = 'section_1_title';
    const SECTION_1_DESCRIPTION = 'section_1_description';
    const SECTION_2_TITLE = 'section_2_title';
    const SECTION_2_DESCRIPTION = 'section_2_description';
    const SECTION_3_TITLE = 'section_3_title';
    const SECTION_3_DESCRIPTION = 'section_3_description';
    const SECTION_4_TITLE = 'section_4_title';
    const SECTION_4_DESCRIPTION = 'section_4_description';
    const SECTION_5_TITLE = 'section_5_title';
    const SECTION_5_DESCRIPTION = 'section_5_description';
    const SECTION_6_TITLE = 'section_6_title';
    const SECTION_6_DESCRIPTION = 'section_6_description';
    const SECTION_7_TITLE = 'section_7_title';
    const SECTION_7_DESCRIPTION = 'section_7_description';
    const SECTION_8_TITLE = 'section_8_title';
    const SECTION_8_DESCRIPTION = 'section_8_description';
    const SECTION_9_TITLE = 'section_9_title';
    const SECTION_9_DESCRIPTION = 'section_9_description';
    const SECTION_10_TITLE = 'section_10_title';
    const SECTION_10_DESCRIPTION = 'section_10_description';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    
    /**
     * Get Author ID
     *
     * @return int|null
     */
    public function getAuthorId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();
    
    /**
     * Get summary
     *
     * @return string|null
     */
    public function getSummary();
    
    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();
    
    /**
     * Get font family
     *
     * @return string|null
     */
    public function getFontFamily();
    
    /**
     * Get bg color
     *
     * @return string|null
     */
    public function getBgColor();

    /**
     * Get header
     *
     * @return string|null
     */
    public function getHeader();

    /**
     * Get footer
     *
     * @return string|null
     */
    public function getFooter();
    
    /**
     * Get section 1 title
     *
     * @return string|null
     */
    public function getSection1Title();
    
    /**
     * Get section 1 description
     *
     * @return string|null
     */
    public function getSection1Description();
    
    /**
     * Get section 2 title
     *
     * @return string|null
     */
    public function getSection2Title();
    
    /**
     * Get section 2 description
     *
     * @return string|null
     */
    public function getSection2Description();

    /**
     * Get section 3 title
     *
     * @return string|null
     */
    public function getSection3Title();
    
    /**
     * Get section 3 description
     *
     * @return string|null
     */
    public function getSection3Description();
    
    /**
     * Get section 4 title
     *
     * @return string|null
     */
    public function getSection4Title();
    
    /**
     * Get section 4 description
     *
     * @return string|null
     */
    public function getSection4Description();
    
    /**
     * Get section 5 title
     *
     * @return string|null
     */
    public function getSection5Title();
    
    /**
     * Get section 5 description
     *
     * @return string|null
     */
    public function getSection5Description();
    
    /**
     * Get section 6 title
     *
     * @return string|null
     */
    public function getSection6Title();
    
    /**
     * Get section 6 description
     *
     * @return string|null
     */
    public function getSection6Description();
    
    /**
     * Get section 7 title
     *
     * @return string|null
     */
    public function getSection7Title();
    
    /**
     * Get section 7 description
     *
     * @return string|null
     */
    public function getSection7Description();
    
    /**
     * Get section 8 title
     *
     * @return string|null
     */
    public function getSection8Title();
    
    /**
     * Get section 8 description
     *
     * @return string|null
     */
    public function getSection8Description();
    
    /**
     * Get section 9 title
     *
     * @return string|null
     */
    public function getSection9Title();
    
    /**
     * Get section 9 description
     *
     * @return string|null
     */
    public function getSection9Description();
    
    /**
     * Get section 10 title
     *
     * @return string|null
     */
    public function getSection10Title();
    
    /**
     * Get section 10 description
     *
     * @return string|null
     */
    public function getSection10Description();

    /**
     * Set ID
     *
     * @param int $id
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setId($id);

    
    /**
     * Set Author ID
     *
     * @param int $author_id
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setAuthorId($author_id);

    /**
     * Set title
     *
     * @param string $title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setTitle($title);

    /**
     * Set content
     *
     * @param string $content
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setContent($content);

    
    /**
     * Set summary
     *
     * @param string $content
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSummary($summary);

    
    /**
     * Set description
     *
     * @param string $content
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setDescription($description);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \MerchBlog\Templates\Api\Data\PostInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setIsActive($isActive);
    
    /**
     * Set font family
     *
     * @param string $font_family
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setFontFamily($font_family);

    /**
     * Set bg color
     *
     * @param string $bg_color
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setBgColor($bg_color);
    
    /**
     * Set header
     *
     * @param string $header
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setHeader($header);
    
    /**
     * Set footer
     *
     * @param string $footer
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setFooter($footer);

    /**
     * Set section 1 title
     *
     * @param string $section1Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection1Title($section1Title);
    
    /**
     * Set section 1 description
     *
     * @param string $section1Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection1Description($section1Description);
    
    /**
     * Set section 2 title
     *
     * @param string $section2Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection2Title($section2Title);
    
    /**
     * Set section 2 description
     *
     * @param string $section2Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection2Description($section2Description);
    
    /**
     * Set section 3 title
     *
     * @param string $section3Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection3Title($section3Title);
    
    /**
     * Set section 3 description
     *
     * @param string $section3Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection3Description($section3Description);
    
    /**
     * Set section 4 title
     *
     * @param string $section4Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection4Title($section4Title);
    
    /**
     * Set section 4 description
     *
     * @param string $section4Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection4Description($section4Description);
    
    /**
     * Set section 5 title
     *
     * @param string $section5Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection5Title($section5Title);
    
    /**
     * Set section 5 description
     *
     * @param string $section5Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection5Description($section5Description);
    
    /**
     * Set section 6 title
     *
     * @param string $section6Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection6Title($section6Title);
    
    /**
     * Set section 6 description
     *
     * @param string $section6Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection6Description($section6Description);
    
    /**
     * Set section 7 title
     *
     * @param string $section7Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection7Title($section7Title);
    
    /**
     * Set section 7 description
     *
     * @param string $section7Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection7Description($section7Description);
    
    /**
     * Set section 8 title
     *
     * @param string $section8Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection8Title($section8Title);
    
    /**
     * Set section 8 description
     *
     * @param string $section8Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection8Description($section8Description);
    
    /**
     * Set section 9 title
     *
     * @param string $section9Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection9Title($section9Title);
    
    /**
     * Set section 9 description
     *
     * @param string $section9Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection9Description($section9Description);
    
    /**
     * Set section 10 title
     *
     * @param string $section10Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection10Title($section10Title);
    
    /**
     * Set section 10 description
     *
     * @param string $section10Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection10Description($section10Description);
}