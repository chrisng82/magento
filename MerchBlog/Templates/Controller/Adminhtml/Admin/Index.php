<?php
      namespace MerchBlog\Templates\Controller\Adminhtml\Admin;

      class Index extends \Magento\Backend\App\Action
      {
        /**
        * @var \Magento\Framework\View\Result\PageFactory
        */
        protected $resultPageFactory;
        // protected $templateFactory;

        /**
         * Constructor
         *
         * @param \Magento\Backend\App\Action\Context $context
         * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
         */
        public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory
        ) {
             parent::__construct($context);
             $this->resultPageFactory = $resultPageFactory;
            //  $this->templateFactory = $templateFactory;
        }

        /**
         * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
         *
         * @return \Magento\Framework\View\Result\Page
         */
        public function execute()
        {
          $resultPage = $this->resultPageFactory->create();

          $resultPage->setActiveMenu('MerchBlog_Templates::admin');
          $resultPage->addBreadcrumb(__('Manage Blog Templates'), __('Manage Blog Templates'));
          $resultPage->getConfig()->getTitle()->prepend(__('Blog Templates'));

          // $template = $this->_templateFactory->create();
          // $collection = $template->getCollection();
          // foreach($collection as $item){
          //   echo "<pre>";
          //   print_r($item->getData());
          //   echo "</pre>";
          // }
          // exit();

          return $resultPage;
        }

          /**
          * Is the user allowed to view the blog post grid.
          *
          * @return bool
          */
          // protected function _isAllowed()
          // {
          //      return $this->_authorization->isAllowed('MerchBlog_Templates::admin');
          // }
      }
    ?>