<?php
namespace MerchBlog\Templates\Model\ResourceModel\Template;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'merchblog_template_collection';
	protected $_eventObject = 'template_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('MerchBlog\Templates\Model\Template', 'MerchBlog\Templates\Model\ResourceModel\Template');
	}

	protected function _initSelect()
	{
		parent::_initSelect();
		
		$this->getSelect()->joinLeft(
			['admin_user' => $this->getTable('admin_user')],
			'main_table.author_id = admin_user.user_id',
			['admin_user.username AS author']
		);
    }
    /**
     * Join store relation table if there is store filter
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
		$this->getSelect2()->joinLeft(
			['admin_user' => $this->getTable('admin_user')],
			'main_table.author_id = admin_user.user_id',
			['admin_user.username AS author']
		);
        parent::_renderFiltersBefore();
	}

    
}