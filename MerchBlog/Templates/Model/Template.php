<?php

namespace MerchBlog\Templates\Model;

use MerchBlog\Templates\Api\Data\TemplateInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Template extends \Magento\Framework\Model\AbstractModel implements IdentityInterface, TemplateInterface
{
    /**#@+
     * Template's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

	const CACHE_TAG = 'merchblog_template';

	protected $_cacheTag = 'merchblog_template';

	protected $_eventPrefix = 'merchblog_template';

	protected function _construct()
	{
		$this->_init('MerchBlog\Templates\Model\ResourceModel\Template');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
    }

    /**
     * Prepare post's statuses.
     * Available event blog_post_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
    * @return int|null
    */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
    * @return int|null
    */
    public function getAuthorId()
    {
        return $this->getData(self::AUTHOR_ID);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get summary
     *
     * @return string|null
     */
    public function getSummary()
    {
        return $this->getData(self::SUMMARY);
    }

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Get font family
     *
     * @return string|null
     */
    public function getFontFamily()
    {
        return $this->getData(self::FONT_FAMILY);
    }
    
    /**
     * Get bg color
     *
     * @return string|null
     */
    public function getBgColor()
    {
        return $this->getData(self::BG_COLOR);
    }

    /**
     * Get header
     *
     * @return string|null
     */
    public function getHeader()
    {
        return $this->getData(self::HEADER);
    }
    
    /**
     * Get footer
     *
     * @return string|null
     */
    public function getFooter()
    {
        return $this->getData(self::FOOTER);
    }
    
    /**
     * Get section1Title
     *
     * @return string|null
     */
    public function getSection1Title()
    {
        return $this->getData(self::SECTION_1_TITLE);
    }
    
    /**
     * Get section1Description
     *
     * @return string|null
     */
    public function getSection1Description()
    {
        return $this->getData(self::SECTION_1_DESCRIPTION);
    }
    
    /**
     * Get section2Title
     *
     * @return string|null
     */
    public function getSection2Title()
    {
        return $this->getData(self::SECTION_2_TITLE);
    }
    
    /**
     * Get section2Description
     *
     * @return string|null
     */
    public function getSection2Description()
    {
        return $this->getData(self::SECTION_2_DESCRIPTION);
    }
    
    /**
     * Get section3Title
     *
     * @return string|null
     */
    public function getSection3Title()
    {
        return $this->getData(self::SECTION_3_TITLE);
    }
    
    /**
     * Get section3Description
     *
     * @return string|null
     */
    public function getSection3Description()
    {
        return $this->getData(self::SECTION_3_DESCRIPTION);
    }
    
    /**
     * Get section4Title
     *
     * @return string|null
     */
    public function getSection4Title()
    {
        return $this->getData(self::SECTION_4_TITLE);
    }
    
    /**
     * Get section4Description
     *
     * @return string|null
     */
    public function getSection4Description()
    {
        return $this->getData(self::SECTION_4_DESCRIPTION);
    }
    
    /**
     * Get section5Title
     *
     * @return string|null
     */
    public function getSection5Title()
    {
        return $this->getData(self::SECTION_5_TITLE);
    }
    
    /**
     * Get section5Description
     *
     * @return string|null
     */
    public function getSection5Description()
    {
        return $this->getData(self::SECTION_5_DESCRIPTION);
    }
    
    /**
     * Get section6Title
     *
     * @return string|null
     */
    public function getSection6Title()
    {
        return $this->getData(self::SECTION_6_TITLE);
    }
    
    /**
     * Get section6Description
     *
     * @return string|null
     */
    public function getSection6Description()
    {
        return $this->getData(self::SECTION_6_DESCRIPTION);
    }
    
    /**
     * Get section7Title
     *
     * @return string|null
     */
    public function getSection7Title()
    {
        return $this->getData(self::SECTION_7_TITLE);
    }
    
    /**
     * Get section7Description
     *
     * @return string|null
     */
    public function getSection7Description()
    {
        return $this->getData(self::SECTION_7_DESCRIPTION);
    }
    
    /**
     * Get section8Title
     *
     * @return string|null
     */
    public function getSection8Title()
    {
        return $this->getData(self::SECTION_8_TITLE);
    }
    
    /**
     * Get section8Description
     *
     * @return string|null
     */
    public function getSection8Description()
    {
        return $this->getData(self::SECTION_8_DESCRIPTION);
    }
    
    /**
     * Get section9Title
     *
     * @return string|null
     */
    public function getSection9Title()
    {
        return $this->getData(self::SECTION_9_TITLE);
    }
    
    /**
     * Get section9Description
     *
     * @return string|null
     */
    public function getSection9Description()
    {
        return $this->getData(self::SECTION_9_DESCRIPTION);
    }
    
    /**
     * Get section10Title
     *
     * @return string|null
     */
    public function getSection10Title()
    {
        return $this->getData(self::SECTION_10_TITLE);
    }
    
    /**
     * Get section10Description
     *
     * @return string|null
     */
    public function getSection10Description()
    {
        return $this->getData(self::SECTION_10_DESCRIPTION);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set Author ID
     *
     * @param int $author_id
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setAuthorId($author_id)
    {
        return $this->setData(self::AUTHOR_ID, $author_id);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set summary
     *
     * @param string $summary
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSummary($summary)
    {
        return $this->setData(self::SUMMARY, $title);
    }

    /**
     * Set description
     *
     * @param string $description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $title);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set creation time
     *
     * @param string $creation_time
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }
    
    /**
     * Set font family
     *
     * @param string $font_family
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setFontFamily($font_family)
    {
        return $this->setData(self::FONT_FAMILY, $font_family);
    }

    /**
     * Set bg color
     *
     * @param string $bg_color
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setBgColor($bg_color) 
    {
        return $this->setData(self::BG_COLOR, $bg_color);
    }
    
    /**
     * Set header
     *
     * @param string $header
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setHeader($header) 
    {
        return $this->setData(self::HEADER, $header);
    }
    
    /**
     * Set footer
     *
     * @param string $footer
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setFooter($footer) 
    {
        return $this->setData(self::FOOTER, $footer);
    }
    
    /**
     * Set section1Title
     *
     * @param string $section1Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection1Title($section1Title) 
    {
        return $this->setData(self::SECTION_1_TITLE, $section1Title);
    }
    
    /**
     * Set section1Description
     *
     * @param string $section1Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection1Description($section1Description) 
    {
        return $this->setData(self::SECTION_1_DESCRIPTION, $section1Description);
    }
    
    /**
     * Set section2Title
     *
     * @param string $section2Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection2Title($section2Title) 
    {
        return $this->setData(self::SECTION_2_TITLE, $section2Title);
    }
    
    /**
     * Set section2Description
     *
     * @param string $section2Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection2Description($section2Description) 
    {
        return $this->setData(self::SECTION_2_DESCRIPTION, $section2Description);
    }
    
    /**
     * Set section3Title
     *
     * @param string $section3Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection3Title($section3Title) 
    {
        return $this->setData(self::SECTION_3_TITLE, $section3Title);
    }
    
    /**
     * Set section3Description
     *
     * @param string $section3Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection3Description($section3Description) 
    {
        return $this->setData(self::SECTION_3_DESCRIPTION, $section3Description);
    }
    
    /**
     * Set section4Title
     *
     * @param string $section4Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection4Title($section4Title) 
    {
        return $this->setData(self::SECTION_4_TITLE, $section4Title);
    }
    
    /**
     * Set section4Description
     *
     * @param string $section4Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection4Description($section4Description) 
    {
        return $this->setData(self::SECTION_4_DESCRIPTION, $section4Description);
    }
    
    /**
     * Set section5Title
     *
     * @param string $section5Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection5Title($section5Title) 
    {
        return $this->setData(self::SECTION_5_TITLE, $section5Title);
    }
    
    /**
     * Set section5Description
     *
     * @param string $section5Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection5Description($section5Description) 
    {
        return $this->setData(self::SECTION_5_DESCRIPTION, $section5Description);
    }
    
    /**
     * Set section6Title
     *
     * @param string $section6Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection6Title($section6Title) 
    {
        return $this->setData(self::SECTION_6_TITLE, $section6Title);
    }
    
    /**
     * Set section6Description
     *
     * @param string $section6Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection6Description($section6Description) 
    {
        return $this->setData(self::SECTION_6_DESCRIPTION, $section6Description);
    }
    
    /**
     * Set section7Title
     *
     * @param string $section7Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection7Title($section7Title) 
    {
        return $this->setData(self::SECTION_7_TITLE, $section7Title);
    }
    
    /**
     * Set section7Description
     *
     * @param string $section7Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection7Description($section7Description) 
    {
        return $this->setData(self::SECTION_7_DESCRIPTION, $section7Description);
    }
    
    /**
     * Set section8Title
     *
     * @param string $section8Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection8Title($section8Title) 
    {
        return $this->setData(self::SECTION_8_TITLE, $section8Title);
    }
    
    /**
     * Set section8Description
     *
     * @param string $section8Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection8Description($section8Description) 
    {
        return $this->setData(self::SECTION_8_DESCRIPTION, $section8Description);
    }
    
    /**
     * Set section9Title
     *
     * @param string $section9Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection9Title($section9Title) 
    {
        return $this->setData(self::SECTION_9_TITLE, $section9Title);
    }
    
    /**
     * Set section9Description
     *
     * @param string $section9Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection9Description($section9Description) 
    {
        return $this->setData(self::SECTION_9_DESCRIPTION, $section9Description);
    }
    
    /**
     * Set section10Title
     *
     * @param string $section10Title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection10Title($section10Title) 
    {
        return $this->setData(self::SECTION_10_TITLE, $section10Title);
    }
    
    /**
     * Set section10Description
     *
     * @param string $section10Description
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setSection10Description($section10Description) 
    {
        return $this->setData(self::SECTION_10_DESCRIPTION, $section10Description);
    }
}