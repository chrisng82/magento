tinyMCE.addI18n({en:{
    noneditablebox:
        {
            insert_noneditable_box : "Insert noneditable box"
        }
}});

(function() {
    tinymce.create('tinymce.plugins.NoneditableBoxPlugin', {
        /**
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {
            var t = this;
            t.editor = ed;
            ed.contentCSS = [ed.settings.magentoPluginsOptions.noneditablebox.css];
            ed.addCommand('mceNoneditablebox', t._insertNoneditableBox, t);
            ed.addButton('noneditablebox', {
                title : 'noneditablebox.insert_noneditable_box',
                cmd : 'mceNoneditablebox',
                image : url + '/img/icon.gif'
            });
        },

        getInfo : function() {
            return {
                longname : 'Demo Text with Box Plugin for TinyMCE 3.x',
                author : 'Marcel Hauri',
                authorurl : 'https://blog.hauri.me/how-to-add-a-tinymce-plugin-to-the-magento2-wysiwyg-editor.html',
                infourl : 'https://github.com/mhauri/magento2-demo-tinymce-plugin',
                version : "1.0"
            };
        },

        _insertNoneditableBox : function () {
            var ed = this.editor;
            ed.execCommand('mceInsertContent', false, '<div class="noneditablebox "<p><br/></p></div><p><br/></p>');
        }
    });

    // Register plugin
    tinymce.PluginManager.add('noneditablebox', tinymce.plugins.NoneditableBoxPlugin);
})();
