tinyMCE.addI18n({en:{
    noneditablebox:
        {
            insert_noneditable_box : "Insert editable box"
        }
}});

(function() {
    tinymce.create('tinymce.plugins.EditableBoxPlugin', {
        /**
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {
            var t = this;
            t.editor = ed;
            ed.contentCSS = [ed.settings.magentoPluginsOptions.editablebox.css];
            ed.addCommand('mceEditablebox', t._insertEditableBox, t);
            ed.addButton('editablebox', {
                title : 'editablebox.insert_editable_box',
                cmd : 'mceEditablebox',
                image : url + '/img/icon.gif'
            });
        },

        getInfo : function() {
            return {
                longname : 'Demo Text with Box Plugin for TinyMCE 3.x',
                author : 'Marcel Hauri',
                authorurl : 'https://blog.hauri.me/how-to-add-a-tinymce-plugin-to-the-magento2-wysiwyg-editor.html',
                infourl : 'https://github.com/mhauri/magento2-demo-tinymce-plugin',
                version : "1.0"
            };
        },

        _insertEditableBox : function () {
            var ed = this.editor;
            ed.execCommand('mceInsertContent', false, '<div class="editablebox "<p><br/></p></div><p><br/></p>');
        }
    });

    // Register plugin
    tinymce.PluginManager.add('editablebox', tinymce.plugins.EditableBoxPlugin);
})();
