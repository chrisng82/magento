<?php

namespace MerchBlog\TinyMcePlugin\Model\Wysiwyg;

use Magento\Framework\DataObject;

class NoneditableBox
{
    const PLUGIN_NAME = 'noneditablebox';

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepo;

    /**
     * NoneditableBox constructor.
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     */
    public function __construct(
        \Magento\Framework\View\Asset\Repository $assetRepo
    ) {
        $this->assetRepo = $assetRepo;
    }

    public function getPluginSettings(DataObject $config) : array
    {
        $plugins = $config->getData('plugins');
        $plugins[] = [
                'name' => self::PLUGIN_NAME,
                'src' => $this->getPluginJsSrc(),
                'options' => [
                    'title' => __('Noneditable Box'),
                    'class' => 'noneditable-box plugin',
                    'css' => $this->getPluginCssSrc()
                ],
            ];

        return ['plugins' => $plugins];
    }

    private function getPluginJsSrc() : string
    {
        return $this->assetRepo->getUrl(
            sprintf('MerchBlog_TinyMcePlugin::js/tiny_mce/plugins/%s/editor_plugin.js', self::PLUGIN_NAME)
        );
    }

    private function getPluginCssSrc() : string
    {
        // return $this->assetRepo->getUrl(
        //     sprintf('MerchBlog_TinyMcePlugin::css/tiny_mce/plugins/%s/content.css', self::PLUGIN_NAME)
        // );
        return $this->assetRepo->getUrl(
            'MerchBlog_TinyMcePlugin::css/tiny_mce/plugins/content.css'
        );
    }
}
