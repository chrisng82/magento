<?php

namespace MerchBlog\TinyMcePlugin\Plugin\Wysiwyg;

use Magento\Cms\Model\Wysiwyg\Config as Subject;
use Magento\Framework\DataObject;
use MerchBlog\TinyMcePlugin\Model\Wysiwyg\NoneditableBox;
use MerchBlog\TinyMcePlugin\Model\Wysiwyg\EditableBox;

class ConfigPlugin
{
    /**
     * @var NoneditableBox
     */
    private $noneditableBox;
    /**
     * @var EditableBox
     */
    private $editableBox;

    /**
     * ConfigPlugin constructor.
     * @param NoneditableBox $noneditableBox
     * @param EditableBox $editableBox
     */
    public function __construct(
        NoneditableBox $noneditableBox,
        EditableBox $editableBox
    ) {
        $this->noneditableBox = $noneditableBox;
        $this->editableBox = $editableBox;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(Subject $subject, DataObject $config) : DataObject
    {
        $noneditableBoxPluginSettings = $this->noneditableBox->getPluginSettings($config);
        $config->addData($noneditableBoxPluginSettings);
        
        $editableBoxPluginSettings = $this->editableBox->getPluginSettings($config);
        $config->addData($editableBoxPluginSettings);
        return $config;
    }
}
