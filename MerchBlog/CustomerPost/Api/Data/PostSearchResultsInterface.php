<?php


namespace MerchBlog\CustomerPost\Api\Data;

interface PostSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Note list.
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface[]
     */
    public function getItems();

    /**
     * Set customer_id list.
     * @param \MerchBlog\CustomerPost\Api\Data\PostInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
