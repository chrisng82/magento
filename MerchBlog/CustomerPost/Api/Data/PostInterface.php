<?php


namespace MerchBlog\CustomerPost\Api\Data;

interface PostInterface
{
 /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const POST_ID       = 'post_id';
    const CUSTOMER_ID   = 'customer_id';
    const AUTHOR_ID     = 'author_id';
    const TEMPLATE_ID   = 'template_id';
    const TITLE         = 'title';
    const SUMMARY       = 'summary';
    const DESCRIPTION   = 'description';
    const CONTENT       = 'content';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const IS_ACTIVE     = 'is_active';
    const SECTION_1     = 'section_1';
    const SECTION_2     = 'section_2';
    const SECTION_3     = 'section_3';
    const SECTION_4     = 'section_4';
    const SECTION_5     = 'section_5';
    const SECTION_6     = 'section_6';
    const SECTION_7     = 'section_7';
    const SECTION_8     = 'section_8';
    const SECTION_9     = 'section_9';
    const SECTION_10    = 'section_10';

    /**
     * Get Post ID
     *
     * @return int|null
     */
    public function getPostId();

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId();
    
    /**
     * Get Author ID
     *
     * @return int|null
     */
    public function getAuthorId();

    /**
     * Get Template ID
     *
     * @return int|null
     */
    public function getTemplateId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();
    
    /**
     * Get summary
     *
     * @return string|null
     */
    public function getSummary();
    
    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();
    
    /**
     * Get section 1
     *
     * @return string|null
     */
    public function getSection1();
    
    /**
     * Get section 2
     *
     * @return string|null
     */
    public function getSection2();
    
    /**
     * Get section 3
     *
     * @return string|null
     */
    public function getSection3();
    
    /**
     * Get section 4
     *
     * @return string|null
     */
    public function getSection4();
    
    /**
     * Get section 5
     *
     * @return string|null
     */
    public function getSection5();
    
    /**
     * Get section 6
     *
     * @return string|null
     */
    public function getSection6();
    
    /**
     * Get section 7
     *
     * @return string|null
     */
    public function getSection7();
    
    /**
     * Get section 8
     *
     * @return string|null
     */
    public function getSection8();
    
    /**
     * Get section 9
     *
     * @return string|null
     */
    public function getSection9();
    
    /**
     * Get section 10
     *
     * @return string|null
     */
    public function getSection10();

    /**
     * Set Post ID
     *
     * @param int $post_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setPostId($post_id);

    
    /**
     * Set Author ID
     *
     * @param int $author_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setAuthorId($author_id);
    
    /**
     * Set Customer ID
     *
     * @param int $customer_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setCustomerId($customer_id);
    
    /**
     * Set Template ID
     *
     * @param int $template_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setTemplateId($template_id);

    /**
     * Set title
     *
     * @param string $title
     * @return \MerchBlog\Templates\Api\Data\TemplateInterface
     */
    public function setTitle($title);

    /**
     * Set content
     *
     * @param string $content
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setContent($content);

    
    /**
     * Set summary
     *
     * @param string $content
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSummary($summary);

    
    /**
     * Set description
     *
     * @param string $content
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setDescription($description);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setIsActive($isActive);

    /**
     * Set section 1
     *
     * @param string $section1
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection1($section1);

    /**
     * Set section 2
     *
     * @param string $section2
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection2($section2);
    
    /**
     * Set section 3
     *
     * @param string $section3
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection3($section3);
    
    /**
     * Set section 4
     *
     * @param string $section4
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection4($section4);
    
    /**
     * Set section 5
     *
     * @param string $section5
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection5($section5);

    /**
     * Set section 6
     *
     * @param string $section6
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection6($section6);

    /**
     * Set section 7
     *
     * @param string $section7
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection7($section7);
    
    /**
     * Set section 8
     *
     * @param string $section8
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection8($section8);
    
    /**
     * Set section 9
     *
     * @param string $section9
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection9($section9);
    
    /**
     * Set section 10
     *
     * @param string $section10
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection10($section10);
}
