<?php


namespace MerchBlog\CustomerPost\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PostRepositoryInterface
{


    /**
     * Save Post
     * @param \MerchBlog\CustomerPost\Api\Data\PostInterface $post
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\MerchBlog\CustomerPost\Api\Data\PostInterface $post);

    /**
     * Retrieve Post
     * @param string $postId
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($postId);

    /**
     * Retrieve Post matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MerchBlog\CustomerPost\Api\Data\PostSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Post
     * @param \MerchBlog\CustomerPost\Api\Data\PostInterface $post
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\MerchBlog\CustomerPost\Api\Data\PostInterface $post);

    /**
     * Delete Note by ID
     * @param string $postId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($postId);
}
