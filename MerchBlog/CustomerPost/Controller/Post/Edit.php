<?php
 
namespace MerchBlog\CustomerPost\Controller\Post;
 
use Magento\Framework\App\Action\Context;
 
class Edit extends \Magento\Framework\App\Action\Action
{
    const REGISTRY_KEY_POST_ID = 'merchblog_post_id';

	protected $_pageFactory;
	protected $_coreRegistry; 
 
    public function __construct(
        Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Framework\Registry $coreRegistry
    )
    {
 
        parent::__construct($context);
		$this->_pageFactory = $pageFactory;
		$this->_coreRegistry = $coreRegistry;
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $params = $this->_request->getParams();
        $id = $params['post_id'];
        if ($id) {
            try {
                $this->_coreRegistry->register(self::REGISTRY_KEY_POST_ID, (int) $id);
                // init model and delete
                // $model = $this->_objectManager->create('MerchBlog\CustomerPost\Model\Post');
                // $model->load($id);
                // $this->_coreRegistry->register('merchblog_customerblog', $model);
                // $this->_coreRegistry->register('merchblog_customerblog', $id);
                $resultPage = $this->_pageFactory->create();
                return $resultPage;      
                // $model->save();
                // display success message
                // $this->messageManager->addSuccessMessage(__('You updated the Post.'));
                // go to grid
                // return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                // return $resultRedirect->setPath('*/*/edit', ['post_id' => $id]);
                // go to grid
                return $resultRedirect->setPath('*/*/');
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Post to edit.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}