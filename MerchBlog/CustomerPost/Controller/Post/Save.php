<?php
 
namespace MerchBlog\CustomerPost\Controller\Post;

class Save extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	protected $_postFactory;
    protected $_customerSession;
 
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\MerchBlog\CustomerPost\Model\PostFactory $postFactory,
        \Magento\Customer\Model\Session $customerSession
		)
	{
		$this->_pageFactory = $pageFactory;
		$this->_postFactory = $postFactory;
		$this->_customerSession = $customerSession;
		return parent::__construct($context);
	}
 
	public function execute()
	{	
        $resultRedirect = $this->resultRedirectFactory->create();
		if ($this->getRequest()->isPost()) {								
			$input = $this->getRequest()->getPostValue();				
			$post = $this->_postFactory->create();				
 
        	if(isset($input['edit_post_id'])){
                $post->getResource()->load($post, $input['edit_post_id']);
				$post->addData($input);
				$post->setIsActive(0); // always set to inactive
				$post->setContent($this->mergeSections($input));
				$post->save();
                // display success message
                $this->messageManager->addSuccessMessage(__('You updated the Post.'));
        	}else{
				$post->setData($input);
				$post->setCustomerId($this->_customerSession->getCustomer()->getId());
				$post->setIsActive(0); // always set to inactive
				$post->setContent($this->mergeSections($input));
				$post->save(); 	
                $this->messageManager->addSuccessMessage(__('You created the Post.'));
        	}
            // go to grid
            return $resultRedirect->setPath('*/*/');
		}
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Post to edit.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
	}	

	private function mergeSections($input)
	{
		$content = '';
		if (!empty($input['section_1']))
		{
			$content = $input['section_1'];
		}
		if (!empty($input['section_2']))
		{
			$content = empty($content) ? $input['section_2'] : $content . '<br/>' . $input['section_2'];
		}
		if (!empty($input['section_3']))
		{
			$content = empty($content) ? $input['section_3'] : $content . '<br/>' . $input['section_3'];
		}
		if (!empty($input['section_4']))
		{
			$content = empty($content) ? $input['section_4'] : $content . '<br/>' . $input['section_4'];
		}
		if (!empty($input['section_5']))
		{
			$content = empty($content) ? $input['section_5'] : $content . '<br/>' . $input['section_5'];
		}
		if (!empty($input['section_6']))
		{
			$content = empty($content) ? $input['section_6'] : $content . '<br/>' . $input['section_6'];
		}
		if (!empty($input['section_7']))
		{
			$content = empty($content) ? $input['section_7'] : $content . '<br/>' . $input['section_7'];
		}
		if (!empty($input['section_8']))
		{
			$content = empty($content) ? $input['section_8'] : $content . '<br/>' . $input['section_8'];
		}
		if (!empty($input['section_9']))
		{
			$content = empty($content) ? $input['section_9'] : $content . '<br/>' . $input['section_9'];
		}
		if (!empty($input['section_10']))
		{
			$content = empty($content) ? $input['section_10'] : $content . '<br/>' . $input['section_10'];
		}
		return $content;
	}
}