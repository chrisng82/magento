<?php

namespace MerchBlog\CustomerPost\Controller\Ajax;

class Template extends \Magento\Framework\App\Action\Action
{
    protected $_resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('template_id');

        // if ($id) {
            // $model->load($id);
            // if (!$model->getId()) {
            //     $this->messageManager->addError(__('This post no longer exists.'));
            //     /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            //     $resultRedirect = $this->resultRedirectFactory->create();

            //     return $resultRedirect->setPath('*/*/');
            // }
        // }
        if ($id)
        {
            $model = $this->_objectManager->create('MerchBlog\Templates\Model\Template');
            $model->load($id);
            //$content = '{{media url="wysiwyg/some_image.jpg"}}';
            //$content = $model->getContent();
            $header = $model->getHeader();
            $footer = $model->getFooter();
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            //$filteredContent = $om->get('Magento\Cms\Model\Template\FilterProvider')->getPageFilter()->filter($content);
            $filteredHeader = $om->get('Magento\Cms\Model\Template\FilterProvider')->getPageFilter()->filter($header);
            $filteredFooter = $om->get('Magento\Cms\Model\Template\FilterProvider')->getPageFilter()->filter($footer);
            $data = [
                'id' => $model->getId(),
                //'content' => $filteredContent,
                'header' => $filteredHeader,
                'footer' => $filteredFooter,
                'font_family' => $model->getFontFamily(),
                'bg_color' => $model->getBgColor(),
                'section_1_title' => $model->getSection1Title(),
                'section_1_description' => $model->getSection1Description(),
                'section_2_title' => $model->getSection2Title(),
                'section_2_description' => $model->getSection2Description(),
                'section_3_title' => $model->getSection3Title(),
                'section_3_description' => $model->getSection3Description(),
                'section_4_title' => $model->getSection4Title(),
                'section_4_description' => $model->getSection4Description(),
                'section_5_title' => $model->getSection5Title(),
                'section_5_description' => $model->getSection5Description(),
                'section_6_title' => $model->getSection6Title(),
                'section_6_description' => $model->getSection6Description(),
                'section_7_title' => $model->getSection7Title(),
                'section_7_description' => $model->getSection7Description(),
                'section_8_title' => $model->getSection8Title(),
                'section_8_description' => $model->getSection8Description(),
                'section_9_title' => $model->getSection9Title(),
                'section_9_description' => $model->getSection9Description(),
                'section_10_title' => $model->getSection10Title(),
                'section_10_description' => $model->getSection10Description(),
            ];
            return $this->_resultJsonFactory->create()->setData($data);
        }
        
        return null;
        

        // /** @var \Magento\Framework\View\Result\Page $resultPage */
        // $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        // /** @var \Magento\Framework\Controller\Result\Raw $response */
        // $response = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        // $response->setHeader('Content-type', 'text/plain');
        // $data1 = 'This is test';
        // $data2 = 20;
        // $response->setContents(
        //     $this->jsonHelper->jsonEncode(
        //         [
        //             'data1' => $data1,
        //             'data2' => $data2,
        //         ]
        //     )
        // );
        // return $response;
    }
}
