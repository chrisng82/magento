<?php
namespace MerchBlog\CustomerPost\Block;

class Create extends \Magento\Framework\View\Element\Template
{
	protected $_pageFactory;
	protected $_postLoader; 
    protected $_templateOptions;
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepo;
 
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
        \MerchBlog\CustomerPost\Model\Source\TemplateOptions $templateOptions,
        \Magento\Framework\View\Asset\Repository $assetRepo
		)
	{
		$this->_pageFactory = $pageFactory;		
        $this->_templateOptions = $templateOptions;	
        $this->_assetRepo = $assetRepo;
		return parent::__construct($context);
	}
 
	public function execute()
	{
		return $this->_pageFactory->create();
	}

    public function getTemplateOptions()
    {
        return $this->_templateOptions->toArray();
    }

    public function getContentHtmlEditor()
    {
        //Refactor this line later
        $object_manager = \Magento\Framework\App\ObjectManager::getInstance();
        $wysiwygConfig = $object_manager->get('\Magento\Cms\Model\Wysiwyg\Config');
        $configwysiwyg =  $wysiwygConfig->getConfig();
        $configwysiwygData = $configwysiwyg->getData();
        $configwysiwygData["settings"]["theme_advanced_buttons1"] = "bold,italic,|,justifyleft,justifycenter,justifyright,|,fontselect,fontsizeselect,|,forecolor,backcolor,|,link,unlink,image,|,bullist,numlist,|,code";
        $configwysiwygData["settings"]["theme_advanced_buttons2"] = false;
        $configwysiwygData["settings"]["theme_advanced_buttons3"] = false;
        $configwysiwygData["settings"]["theme_advanced_buttons4"] = false;
        $configwysiwygData["settings"]["theme_advanced_statusbar_location"] = false;
        $configwysiwygData["height"] = "250px";
        $configwysiwygData["add_variables"] = false;
        $configwysiwygData["plugins"] = false;
        $configwysiwygData["add_widgets"] = false;
        $configwysiwygData["add_images"] = false;
        $configwysiwygData["files_browser_window_url"] =false;
        $configwysiwygData["no_display"] =true;
        $configwysiwygData["toggle_button"] = false;
        $configwysiwyg->setData($configwysiwygData);
        $elementId = "content";
        $config = [
            'label'     => __('Content'),
            'name'      => 'content',
            'config' => $configwysiwyg,
            'wysiwyg' =>  true,
            'style' => 'width:100%; height:250px;',
            'required'=> true,
            'class' => " required-entry",
            'value' => "",
            "validation" => [
                "required-entry" => true
            ]
        ];
        $form = $object_manager->get('\Magento\Framework\Data\Form');
        $editor = $object_manager->get('\Magento\Framework\Data\Form\Element\Editor')->setData($config);
        $editor->setForm($form);
        $editor->setId($elementId);
        return $editor->getElementHtml();
    }

    public function getCssUrl() {
        return $this->_assetRepo->getUrl(
            'MerchBlog_CustomerPost::css/content.css'
        );
    }
}