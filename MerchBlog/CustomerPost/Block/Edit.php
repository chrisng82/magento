<?php
namespace MerchBlog\CustomerPost\Block;

use \MerchBlog\CustomerPost\Controller\Post\Edit as EditAction;

class Edit extends \Magento\Framework\View\Element\Template
{
	protected $_pageFactory;
    protected $_coreRegistry;
    /**
     * PostFactory
     * @var null|PostFactory
     */
    protected $_postFactory = null;
    /**
     * Post
     * @var null|Post
     */
    protected $_post = null;

    protected $_templateOptions;
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepo;
    
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
    \Magento\Framework\View\Result\PageFactory $pageFactory,
    \Magento\Framework\Registry $coreRegistry,
    \MerchBlog\CustomerPost\Model\PostFactory $postFactory,
    \MerchBlog\CustomerPost\Model\Source\TemplateOptions $templateOptions,
    \Magento\Framework\View\Asset\Repository $assetRepo,
    array $data = [])
	{
		$this->_pageFactory = $pageFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_postFactory = $postFactory;
        $this->_templateOptions = $templateOptions;
        $this->_assetRepo = $assetRepo;
		parent::__construct($context, $data);
    }

    public function execute()
	{
		return $this->_pageFactory->create();
	}
 
	// public function getEditRecord()
    // {
    //     $model = $this->_coreRegistry->registry('merchblog_customerblog');    	
    //     $result = $model->getData();       
    //     return $result;
    // }

    /**
     * Lazy loads the requested post
     * @return Post
     * @throws LocalizedException
     */
    public function getPost()
    {
        if ($this->_post === null) {
            /** @var Post $post */
            $post = $this->_postFactory->create();
            $post->load($this->_getPostId());
            // $post->getResource()->load($post, $this->_getPostId());
            if (!$post->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Post not found'));
            }

            $this->_post = $post;
        }
        return $this->_post;
    }

    /**
     * Retrieves the post id from the registry
     * @return int
     */
    protected function _getPostId()
    {
        return (int) $this->_coreRegistry->registry(
            EditAction::REGISTRY_KEY_POST_ID
        );
    }

    public function getTemplateOptions()
    {
        return $this->_templateOptions->toArray();
    }

    public function getContentHtmlEditor()
    {
        //Refactor this line later
        $object_manager = \Magento\Framework\App\ObjectManager::getInstance();
        $wysiwygConfig = $object_manager->get('\Magento\Cms\Model\Wysiwyg\Config');
        $configwysiwyg =  $wysiwygConfig->getConfig();
        $configwysiwygData = $configwysiwyg->getData();
        $configwysiwygData["theme"] = "advanced";
        $configwysiwygData["theme_advanced_buttons1"] = "bold,italic,|,justifyleft,justifycenter,justifyright,|,fontselect,fontsizeselect,|,forecolor,backcolor,|,link,unlink,image,|,bullist,numlist,|,code,media,image";
        $configwysiwygData["theme_advanced_buttons2"] = "";
        $configwysiwygData["theme_advanced_buttons3"] = "";
        $configwysiwygData["theme_advanced_buttons4"] = "";
        $configwysiwygData["theme_advanced_toolbar_location"] = "top";
        $configwysiwygData["theme_advanced_toolbar_align"] = "left";
        $configwysiwygData["theme_advanced_statusbar_location"] = "bottom";
        $configwysiwygData["height"] = "350px";
        $configwysiwygData["add_variables"] = false;
        $configwysiwygData["plugins"] = ["noneditable"];
        $configwysiwygData["add_widgets"] = false;
        $configwysiwygData["add_images"] = true;
        $configwysiwygData["files_browser_window_url"] =false;
        $configwysiwygData["no_display"] = true;
        $configwysiwygData["toggle_button"] = false;
        $configwysiwygData['add_directives'] = true;
        // $configwysiwygData['use_container'] = true;
        $configwysiwygData["extended_valid_element"] = "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]";
        
        $content_css = $this->_assetRepo->getUrl(
            'MerchBlog_TinyMcePlugin::js/tiny_mce/plugins/noneditablebox/editor_plugin.js'
        );
        $configwysiwygData['content_css'] = $content_css;
        
        $configwysiwyg->setData($configwysiwygData);
        $elementId = "content";
        $config = [
            'label'     => __('Content'),
            'name'      => 'content',
            'config' => $configwysiwyg,
            'wysiwyg' =>  true,
            'style' => 'width:100%; height:250px;',
            'required'=> true,
            'class' => " required-entry",
            'value' => $this->_post->getContent(),
            "validation" => [
                "required-entry" => true
            ]
        ];
        $form = $object_manager->get('\Magento\Framework\Data\Form');
        $editor = $object_manager->get('\Magento\Framework\Data\Form\Element\Editor')->setData($config);
        $editor->setForm($form);
        $editor->setId($elementId);
        return $editor->getElementHtml();
    }

    public function getCssUrl() {
        return $this->_assetRepo->getUrl(
            'MerchBlog_CustomerPost::css/content.css'
        );
    }
}