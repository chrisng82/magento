<?php


namespace MerchBlog\CustomerPost\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'MerchBlog\CustomerPost\Model\Post',
            'MerchBlog\CustomerPost\Model\ResourceModel\Post'
        );
    }
}
