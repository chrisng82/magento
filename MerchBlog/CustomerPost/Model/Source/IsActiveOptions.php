<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MerchBlog\CustomerPost\Model\Source;

/**
 * @api
 * @since 100.0.2
 */
class IsActiveOptions implements \Magento\Framework\Option\ArrayInterface
{
    protected $_post;

    public function __construct(
        \MerchBlog\CustomerPost\Model\Post $post
    ) {
        $this->_post = $post;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_post->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}