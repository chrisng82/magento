<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MerchBlog\CustomerPost\Model\Source;

/**
 * @api
 * @since 100.0.2
 */
class TemplateOptions implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_resource = $resource;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $optionArray = [];
         foreach ($this->getTemplates() as $field) {
            $optionArray[] = ['label' => ($field['title'] . ' - ' . $field['description']), 'value' => $field['id']];
        }
        return $optionArray;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $option = [];
         foreach ($this->getTemplates() as $field) {
            $option[$field['id']] = $field['title'] . ' - ' . $field['description'];
        }
        return $option;
    }

    public function getTemplates()
    {
        $adapter = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $select = $adapter->select()
                    ->from('merchblog_templates');
        return $adapter->fetchAll($select);
    }
}