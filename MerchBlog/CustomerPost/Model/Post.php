<?php


namespace MerchBlog\CustomerPost\Model;

use MerchBlog\CustomerPost\Api\Data\PostInterface;

class Post extends \Magento\Framework\Model\AbstractModel implements PostInterface
{

    /**#@+
     * Template's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

	const CACHE_TAG = 'merchblog_customerpost';

	protected $_cacheTag = 'merchblog_customerpost';

	protected $_eventPrefix = 'merchblog_customerpost';

	protected function _construct()
	{
        $this->_init('MerchBlog\CustomerPost\Model\ResourceModel\Post');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
    }

    /**
     * Prepare post's statuses.
     * Available event blog_post_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Pending Approval')];
    }

    /**
    * @return int|null
    */
    public function getPostId()
    {
        return $this->getData(self::POST_ID);
    }

    /**
    * @return int|null
    */
    public function getAuthorId()
    {
        return $this->getData(self::AUTHOR_ID);
    }

    /**
    * @return int|null
    */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
    * @return int|null
    */
    public function getTemplateId()
    {
        return $this->getData(self::TEMPLATE_ID);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get summary
     *
     * @return string|null
     */
    public function getSummary()
    {
        return $this->getData(self::SUMMARY);
    }

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }
    
    /**
     * Get section 1
     *
     * @return string|null
     */
    public function getSection1()
    {
        return $this->getData(self::SECTION_1);
    }
    
    /**
     * Get section 2
     *
     * @return string|null
     */
    public function getSection2()
    {
        return $this->getData(self::SECTION_2);
    }

    /**
     * Get section 3
     *
     * @return string|null
     */
    public function getSection3()
    {
        return $this->getData(self::SECTION_3);
    }

    /**
     * Get section 4
     *
     * @return string|null
     */
    public function getSection4()
    {
        return $this->getData(self::SECTION_4);
    }

    /**
     * Get section 5
     *
     * @return string|null
     */
    public function getSection5()
    {
        return $this->getData(self::SECTION_5);
    }

    /**
     * Get section 6
     *
     * @return string|null
     */
    public function getSection6()
    {
        return $this->getData(self::SECTION_6);
    }
    
    /**
     * Get section 7
     *
     * @return string|null
     */
    public function getSection7()
    {
        return $this->getData(self::SECTION_7);
    }
    
    /**
     * Get section 8
     *
     * @return string|null
     */
    public function getSection8()
    {
        return $this->getData(self::SECTION_8);
    }
    
    /**
     * Get section 9
     *
     * @return string|null
     */
    public function getSection9()
    {
        return $this->getData(self::SECTION_9);
    }
    
    /**
     * Get section 10
     *
     * @return string|null
     */
    public function getSection10()
    {
        return $this->getData(self::SECTION_10);
    }

    /**
     * Set Post ID
     *
     * @param int $post_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setPostId($post_id)
    {
        return $this->setData(self::POST_ID, $post_id);
    }
    /**
     * Set Customer ID
     *
     * @param int $customer_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setCustomerId($customer_id)
    {
        return $this->setData(self::CUSTOMER_ID, $customer_id);
    }

    /**
     * Set Author ID
     *
     * @param int $author_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setAuthorId($author_id)
    {
        return $this->setData(self::AUTHOR_ID, $author_id);
    }

    /**
     * Set Template ID
     *
     * @param int $template_id
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setTemplateId($template_id)
    {
        return $this->setData(self::TEMPLATE_ID, $template_id);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set summary
     *
     * @param string $summary
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSummary($summary)
    {
        return $this->setData(self::SUMMARY, $title);
    }

    /**
     * Set description
     *
     * @param string $description
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $title);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set creation time
     *
     * @param string $creation_time
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }
    
    /**
     * Set section 1
     *
     * @param string $section1
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection1($section1)
    {
        return $this->setData(self::SECTION_1, $section1);
    }
    
    /**
     * Set section 2
     *
     * @param string $section2
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection2($section2)
    {
        return $this->setData(self::SECTION_2, $section2);
    }
    
    /**
     * Set section 3
     *
     * @param string $section3
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection3($section3)
    {
        return $this->setData(self::SECTION_3, $section3);
    }
    
    /**
     * Set section 4
     *
     * @param string $section4
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection4($section4)
    {
        return $this->setData(self::SECTION_4, $section4);
    }
    
    /**
     * Set section 5
     *
     * @param string $section5
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection5($section5)
    {
        return $this->setData(self::SECTION_5, $section5);
    }
    
    /**
     * Set section 6
     *
     * @param string $section6
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection6($section6)
    {
        return $this->setData(self::SECTION_6, $section6);
    }
    
    /**
     * Set section 7
     *
     * @param string $section7
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection7($section7)
    {
        return $this->setData(self::SECTION_7, $section7);
    }
    
    /**
     * Set section 8
     *
     * @param string $section8
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection8($section8)
    {
        return $this->setData(self::SECTION_8, $section8);
    }
    
    /**
     * Set section 9
     *
     * @param string $section9
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection9($section9)
    {
        return $this->setData(self::SECTION_9, $section9);
    }
    
    /**
     * Set section 10
     *
     * @param string $section10
     * @return \MerchBlog\CustomerPost\Api\Data\PostInterface
     */
    public function setSection10($section10)
    {
        return $this->setData(self::SECTION_10, $section10);
    }
}
