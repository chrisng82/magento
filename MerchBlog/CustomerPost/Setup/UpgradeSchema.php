<?php
namespace MerchBlog\CustomerPost\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		
        $installer = $setup;
        $setup->startSetup();

        $version = $context->getVersion();
        $connection = $setup->getConnection();
        
        if (version_compare($version, '1.1.0') < 0) {
            /* Add author field to posts table */
            $connection->addColumn(
                $setup->getTable('magefan_blog_post'),
                'customer_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'Customer ID',
                ]
            );

            $connection->addIndex(
                $setup->getTable('magefan_blog_post'),
                $setup->getIdxName($setup->getTable('magefan_blog_post'), ['customer_id']),
                ['customer_id']
            );
        }

        if (version_compare($version, '1.2.0') < 0) {
            /* Add template field to posts table */
            $connection->addColumn(
                $setup->getTable('magefan_blog_post'),
                'template_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'Template ID',
                ]
            );
        }

        if (version_compare($version, '1.3.0') < 0) {
            for ($i=1; $i < 11; $i++) { 
                /* Add content field to posts table */
                $connection->addColumn(
                    $setup->getTable('magefan_blog_post'),
                    'section_'.$i,
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '64k',
                        'nullable' => true,
                        'comment' => 'Post section '.$i.' content',
                    ]
                );
            }
        }

        if (version_compare($version, '1.4.0') < 0) {
            /* Add font and background color to template table */
            $connection->addColumn(
                $setup->getTable('merchblog_templates'),
                'font_family',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => true,
                    'comment' => 'Blog template font family',
                ]
            );
            $connection->addColumn(
                $setup->getTable('merchblog_templates'),
                'bg_color',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 20,
                    'nullable' => true,
                    'comment' => 'Blog template background color',
                ]
            );
        }
        
        if (version_compare($version, '1.5.0') < 0) {
            
            /* Add header & footer field to template table */
            $connection->addColumn(
                $setup->getTable('merchblog_templates'),
                'header',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '64k',
                    'nullable' => true,
                    'comment' => 'Template header content',
                ]
            );

            $connection->addColumn(
                $setup->getTable('merchblog_templates'),
                'footer',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '64k',
                    'nullable' => true,
                    'comment' => 'Template footer content',
                ]
            );

            for ($i=1; $i < 11; $i++) { 
                /* Add section title and description field to template table */
                $connection->addColumn(
                    $setup->getTable('merchblog_templates'),
                    'section_'.$i.'_title',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 80,
                        'nullable' => true,
                        'comment' => 'Blog template section '.$i.' title',
                    ]
                );
                $connection->addColumn(
                    $setup->getTable('merchblog_templates'),
                    'section_'.$i.'_description',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'Blog template section '.$i.' description',
                    ]
                );
            }
        }

		$installer->endSetup();
	}
}