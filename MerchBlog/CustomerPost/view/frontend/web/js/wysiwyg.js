require([
    'jquery',
    'mage/adminhtml/wysiwyg/tiny_mce/setup'
], function(jQuery){

var config = {}, 
    editor;

jQuery.extend(config, {
    settings: {
        theme_advanced_buttons1 : 'bold,italic,|,justifyleft,justifycenter,justifyright,|,' +
                                    'fontselect,fontsizeselect,|,forecolor,backcolor,|,link,unlink,image,|,bullist,numlist,|,code,media,image',
        theme_advanced_buttons2: 'bold,italic,|,justifyleft,justifycenter,justifyright,|,',
        theme_advanced_buttons3: null,
        theme_advanced_buttons4: null
    }
});
editor = new tinyMceWysiwygSetup(
    'content',
    config
);
editor.turnOn();
jQuery('#content')
    .addClass('wysiwyg-editor')
    .data(
        'wysiwygEditor',
        editor
    );
});