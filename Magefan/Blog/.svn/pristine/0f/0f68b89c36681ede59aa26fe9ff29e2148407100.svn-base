<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * See LICENSE.txt for license details (http://opensource.org/licenses/osl-3.0.php).
 *
 * Glory to Ukraine! Glory to the heroes!
 */

namespace Magefan\Blog\Block\Adminhtml\Grid\Column\Render;

/**
 * Customer column renderer
 */
class Customer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @var \Magefan\Blog\Model\CategoryFactory
     */
    protected $customerFactory;

    /**
     * @var array
     */
    static protected $customers = [];

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param \Magefan\Blog\Model\CustomerFactory $localeLists
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magefan\Blog\Model\CustomerFactory $customerFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerFactory = $customerFactory;
    }

    /**
     * Render category grid column
     *
     * @param   \Magento\Framework\DataObject $row
     * @return  string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        if ($id = $row->getData($this->getColumn()->getIndex())) {
            $title = $this->getCustomerById($id)->getTitle();
            return $title;
        }
        return null;
    }

    /**
     * Retrieve customer by id
     *
     * @param   int $id
     * @return  \Magefan\Blog\Model\Customer
     */
    protected function getCustomerById($id)
    {
        if (!isset(self::$customers[$id])) {
            self::$customers[$id] = $this->customerFactory->create()->load($id);
        }
        return self::$customers[$id];
    }
}
