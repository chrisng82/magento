<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * See LICENSE.txt for license details (http://opensource.org/licenses/osl-3.0.php).
 *
 * Glory to Ukraine! Glory to the heroes!
 */

namespace Magefan\Blog\Block\Adminhtml\Grid\Column;

/**
 * Admin blog grid customer
 */
class Customer extends \Magento\Backend\Block\Widget\Grid\Column
{
    /**
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_rendererTypes['customer'] = 'Magefan\Blog\Block\Adminhtml\Grid\Column\Render\Customer';
        //$this->_filterTypes['customer'] = 'Magefan\Blog\Block\Adminhtml\Grid\Column\Filter\Customer';
    }
}
